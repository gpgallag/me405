'''
@file encoderDriver.py

@brief This file contains a driver for an optical encoder in quadrature.

@details The driver is initialized with the requisite timer, timer channels, and pin
locations for the Nucleo L476RG microprocessor, as inputted by the user. The driver
also corrects for under/overflow.

@package Lab0x09
@package Lab0x08

@author Grant Gallagher

@author Mitchell Carroll

@date March 11, 2021
'''
import pyb

class EncoderDriver():
    '''
    @brief      Encapsulates the operation of a timer to read from an encoder.
    
    @details    This class initializes the pin locations, timer, and timer
                channels (for each pin) for an encoder. The class reads the
                position of the encoder from two optical sensors in quadrature
                while correcting for under/overflow. A run task is created to
                constantly update the encoder position and delta at a regular
                interval to avoid missing encoder ticks.
    '''
    def __init__(self, IN1, IN2, timer, debug):
        '''
        @brief       Constructs an EncoderDriver object.
        
        @param pin_A (1/2) of the signal pins for the optical sensor on the encoder.
        @param pin_B (2/2) of the signal pins for the optical sensor on the encoder.
        @param timer A timer object with prescalar and period used to construct the timer channels.
        @param debug A boolean value that determines debugging state.
        '''
        ## The timer used to measure encoder counts.
        self.timer = timer
        
        ## Timer channel attribute initialized to pin_A.
        self.timer_ch1 = self.timer.channel(IN1['Channel'], pyb.Timer.ENC_AB, pin=IN1['Pin'])
        
        ## Timer channel attribute initialized to pin_B.
        self.timer_ch2 = self.timer.channel(IN2['Channel'], pyb.Timer.ENC_AB, pin=IN2['Pin'])
        
        ## The most current 'tick' of the encoder position.
        self.current_tick = 0
        
        ## Second most recent recorded encoder position.
        self.position_old = 0
        
        ## Most recent recorded encoder position.
        self.position_new = 0
        
        ## The count difference between position_new and position_old.
        self.delta = 0
        
        ## The debug state (bool).
        self.debug = debug
        
        if self.debug: # When debug == true
            print('Creating an Encoder Driver Object')
        
        
    def update(self):
        '''
        @brief Corrects under/overflow for the encounter count and advances delta, most recent position, and second most recent position.
        '''           
        self.delta = self.timer.counter() - (self.position_new) # Difference between last and current position
        if self.delta > 0xFFFF/2: # Check for underflow
            self.delta -= (0xFFFF + 1)                    # Fixes underflow (for 16-bit encoder)
            
        elif self.delta < -0xFFFF/2: # Check for overflow
            self.delta += (0xFFFF + 1)                    # Fixes overflow (for 16-bit encoder)
            
        else:
            pass
        
        self.position_old = self.position_new               # Advance second most recent encoder position
        self.position_new  = self.position_old + self.delta # Advance most recent coder position
        
        if self.debug: # When debug == true
            print('Raw position (tim.counter): ' + str(self.timer.counter()))
            print('Encoder Position:           ' + str(self.position_new))
            print('Encoder Delta:              ' + str(self.delta))
            print('\n')
    
    
    def get_position(self):
        '''
        @brief   Returns the absolute value of the encoder position since initialization or zero-ing -- accounting for over/underflow.
        @return  The most recent value of the encoder position.
        '''
        return self.position_new
    
    
    def get_old_position(self):
        '''
        @brief   Returns the second most recent absolute value of the encoder position since initialization or zero-ing -- accounting for over/underflow.
        @return  The second most recent value of the encoder position.
        '''
        return self.position_old
        
    
    def set_position(self, setValue):  
        '''
        @brief          Sets all forms of the current encoder position to a specified value.
        @param setValue An integer value for the encoder position (count) to be set to.
                        By default, setValue is 0 in order to zero the encoder.
        ''' 
        self.position_old = setValue 
        self.position_new = setValue
        self.timer.counter(setValue)
    
    
    def get_delta(self):
        '''
        @brief      Updates and returns the difference in encoder position.
        @return     The difference between position_new and position_old.
        '''
        return self.delta

# The following block of code is an example of how to enable and drive both EncoderDriver objects.   
if __name__ == '__main__':
    
    ## A dictionary item containing the pin address and channel number for the E1_CH1 input pin.
    E1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B6),
              'Channel': 1 }
    ## A dictionary item containing the pin address and channel number for the E1_CH2 input pin.
    E1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B7),
              'Channel': 2 }
    ## A timer object using timer 4 with a period of 65535.
    tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF)
    ## An EncoderDriver object used to measure the count and position of Encoder 1.
    enc1 = EncoderDriver(E1_CH1, E1_CH2, tim4, debug=True)
    
    
    ## A dictionary item containing the pin address and channel number for the E2_CH1 input pin.
    E2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.C6),
              'Channel': 1 }
    ## A dictionary item containing the pin address and channel number for the E2_CH2 input pin.
    E2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.C7),
              'Channel': 2 }
    ## A timer object using timer 8 with a period of 65535.
    tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF)
    ## An EncoderDriver object used to measure the count and position of Encoder 1.
    enc2 = EncoderDriver(E2_CH1, E2_CH2, tim8, debug=True)

    ## The time in milliseconds between performing task functions: measuring encoder data.
    interval = 20
    
    ## The time in milliseconds since epoch.
    start_time = pyb.millis()
    ## The next time to run the task functions.
    next_time = interval + pyb.elapsed_millis(start_time) 
    
    ## The measured RPM of Encoder 1.
    W_meas_1 = 0
    
    ## The measured RPM of Encoder 2.
    W_meas_2 = 0

    # Run forever
    while True:
        # Check for next run time.
        if (pyb.elapsed_millis(start_time) >= next_time):
            # Update the encoder positions/deltas
            enc1.update()
            enc2.update()
            
            # Note: velocity must be measured OUTSIDE of the EncoderDriver class, so as to operate properly with task timing.
            W_meas_1 = enc1.get_delta() * 10**3 * 60 / (interval * 4000) # Calculate motor speed (rpm)
            print("Enc1 RPM = " + str(W_meas_1))
            
            W_meas_2 = enc1.get_delta() * 10**3 * 60 / (interval * 4000) # Calculate motor speed (rpm)
            print("Enc1 RPM = " + str(W_meas_2))
        
            next_time += interval # Update the "Scheduled" timestamp