'''
@file UI_Front.py

@brief A front-end user interface FSM.

@details A front-end user interface that runs locally on a PC terminal and receives user input.
         When the user presses \‘G,\’ the Nucleo™ back-end awaits a button press.
         When data collection is complete, the script generates both a plot of voltage vs.time
         (using matplotlib) and a .CSV file of data (timestamps and ADC counts).

@package Lab0x03

@brief This package contains main0x03.py and UI_Front.py.
       
@author Grant Gallagher

@date February 4, 2021
'''

import serial
import numpy as np
import matplotlib.pyplot as plt

class UI_Front:
    '''
    @brief   A front-end user interface FSM class.
    @details A front-end user interface FSM class that is used to collect the
             voltage step response of a button press on a Nucleo. Upon startup,
             the task awaits the user to press 'G' to begin data collection.
             Then, the UI waits for the button to be pressed, and data to be
             sent through the UART, where it is then stored as a .CSV and plotted
             using matplotlib.
    '''
    ## State 0: Initialization
    S0_INIT = 0    
    
    ## State 1: Wait for a response from the Nucleo
    S1_WAIT_FOR_RESPONSE  = 1
        
    ## State 2: Receive/Process data sent by the Nucleo
    S2_RECIEVE_DATA = 2
    
    ## State 3: Store/Export the data as a .CSV file and .PNG plot
    S3_EXPORT_DATA = 3
    

    def __init__(self):
        '''
        @brief The constructor for the UI_Front task object.
        '''
        ## The serial port used to communicate with the Nucleo.
        self.ser = serial.Serial(port='COM5',baudrate=115373,timeout=1)
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A string that holds the command to be sent to the Nucleo.
        self.cmd = ''
        
        ## A string that holds the response sent by the Nucleo.
        self.resp = ''       
        
        ## An array that holds the timestamps associated with ADC count signals.
        self.times = np.array([], dtype = np.float32)
        
        ## An array for the step size of the step response data.
        self.step = np.array([], dtype = np.float32)
        
        ## An array that holds the ADC signal count data.
        self.signal_counts = np.array([], dtype = np.float32)
        
    def run(self):
        '''
        @brief A FSM that continuously iterates through task commands.
        '''      
        # Run forever (or until Exception/Break)
        while True:
            # State 0 Code
            if(self.state == self.S0_INIT):
                print('When you are ready to start, enter \'G\'.\n')  
                self.cmd = input('')
                
                if ((self.cmd == 'g') or (self.cmd == 'G')):
                    self.ser.write('G'.encode('ascii'))
                    self.transitionTo(self.S1_WAIT_FOR_RESPONSE)
                else:
                    print('That was not a valid input. Enter \'G\' to start.')
                    pass
                
                
            # State 1 Code
            elif(self.state == self.S1_WAIT_FOR_RESPONSE):
                self.resp = str(self.ser.readline().decode('ascii')).strip()
                
                if self.resp != '': # Checks for non-empty response
                    if self.resp == 'E':
                        print('There has been some sort of error. Let\'s try again.')
                        self.transitionTo(self.S0_INIT)
                    elif self.resp == 'START':
                        self.transitionTo(self.S2_RECIEVE_DATA)
                    else:
                        print(self.resp)
                        pass
                    
            # State 2 Code      
            elif(self.state == self.S2_RECIEVE_DATA):
                self.resp = self.ser.readline().decode('ascii').strip() # Decode the response from the UI back-end
                
                # End of data transmission
                if self.resp == 'END':
                    self.transitionTo(self.S3_EXPORT_DATA)
                # Iterate through collected data
                else:
                    data_point = self.resp.strip().split(',')
                    self.times = np.append(self.times, float(data_point[0])) # Append time array with datapoint
                    self.signal_counts = np.append(self.signal_counts, float(data_point[1])) # Append signal array with datapoint
            
            # State 3 Code
            elif(self.state == self.S3_EXPORT_DATA):                          
                self.exportData() # Export the data as a .CSV
                self.exportPlot() # Plot and export the data as a .PNG of ADC count vs time
                break
    
            # Error handling
            else:
                pass

    def exportPlot(self):
        '''
        @brief Creates and exports a plot for the button step response.
        @details Labels the y-axis (Signal Voltage), x-axis (time), and title of the plot, and exports it as a "ADC_plot.png".
        '''
        plt.plot(self.times, self.signal_counts, 'k.')          # Plots the ADC count and time
        plt.xlabel('Time, t [ms]')                              # Label x-axis
        plt.ylabel('Signal Voltage, V [ADC count]')             # Label y-axis
        plt.title('Step Response of User Button Press Voltage') # Label plot title
        plt.legend(['Signal Data'])                             # Label datapoints        
        plt.savefig('ADC_plot.png')                             # Save the plot under file name
        print('The step response plot has been exported as: "ADC_plot.png"')
        
        
    def exportData(self):
        '''
        @brief Exports the raw signal response data as a .CSV file to the current folder.
        @details The raw data is exported as "ADC_data.csv".
        '''
        # Opens a new .CSV file
        with open('ADC_data.csv','w') as file:
                    # Iterate through rows of data
                    for index in range(len(self.times)):
                        file.write('{:f},{:f}\n'.format(self.times[index],self.signal_counts[index])) # Write a row of a data
                        
        print('The ADC step response data has been exported as: "ADC_data.csv"')

        
    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState


if __name__ == '__main__':    
    ## A UI_Front task object
    task = UI_Front()
    
    task.run() # Continuously run the FSM