'''
@file main0x03.py

@brief A back-end user interface and data-collection FSM.

@details A back-end user interface that runs remotely on a Nucleo. This script
         is named "main.py" so that it is the first/only program to run upon booting
         the Nucleo. When ran, the UI will wait for communication from the front-end
         begin data collection. Upon reciept of the character \'G,\' the FSM
         will wait for the user to press the blue button. Then, an ISR will be called
         to record the voltage (ADC count) and relative timestamps (ms) of the step
         response. Finally, the data will be sent back to the UI front-end through the
         UART will it will be processed and exported as a .CSV file and .PNG plot.

@package Lab0x03

@author Grant Gallagher

@date February 4, 2021
'''
import pyb
from pyb import UART
import array
import micropython

class UI_Back: 
    '''
    @brief   A back-end FSM used to collect voltage response of a user button press.
    @details A back-end user interface and data collection FSM class that is used to collect the
             voltage step response of a button press on a Nucleo. Upon startup,
             the task awaits character \'G\' from the front-end to begin data collection.
             Then, the FSM waits for the button to be pressed. Once pressed, an ISR will be
             called and the step response will be recorded. Finally, the data will be
             sent through the UART, where it is then stored as a .CSV and exported
             as a .PNG.
    '''
    ## State 0: Initialization
    S0_INIT = 0
    
    ## State 1: Wait for user input
    S1_WAIT_FOR_COMMAND = 1
    
    ## State 2: Collect button step response data
    S2_COLLECT_DATA = 2
    
    ## State 3: Send data to UI front-end
    S3_SEND_DATA = 3
       
    
    def __init__(self):
        '''
        @brief The constructor for the UI_Back task object.
        '''   
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The UART console object.
        self.uart = UART(2)
        
        ## The pin object associated with the user-button output on the Nucleo.
        self.button = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)
        
        ## The pin object associated with the analog-to-digital converter (ADC) on the Nucleo.
        self.ADC1 = pyb.ADC(pyb.Pin.board.PA0)
        
        ## A boolean indicating whether user button has been pressed.
        self.isPressed = 0
        
        ## An array that holds the signal count data from the button press as an unsigned short.
        self.buffy = array.array('H', (0 for i in range(200)))
        
        ## A timer object initialized to timer 5, with a frequency of 40 Khz.
        self.adc_tim = pyb.Timer(5, freq = 40000)
        
        ## A string that holds the user input sent from the UI front-end.
        self.resp = ''
        
        ## The external interrupt object for the user button that activates on the falling edge.
        self.extint = pyb.ExtInt(self.button, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.callback)

    def run(self):
        '''
        @brief A FSM that continuously iterates through task commands.
        '''      
        while True:
            # Run State 0 Code   
            if(self.state == self.S0_INIT):
                ## An emergency buffer to store errors that are thrown inside ISR.
                micropython.alloc_emergency_exception_buf(200)
                
                self.transitionTo(self.S1_WAIT_FOR_COMMAND)
             
            # Run State 1 Code
            elif(self.state == self.S1_WAIT_FOR_COMMAND):
                # Check for user input
                if self.uart.any() != 0:
                    self.resp = self.uart.readline().decode('ascii') # Store user input
                    # User chooses to begin data collection.
                    if self.resp == 'G':
                        self.isPressed = 0
                        print('Press the blue button to begin.')
                        self.transitionTo(self.S2_COLLECT_DATA) 
                    # Error handling
                    else:
                        pass
                    
            # Run State 2 Code    
            elif(self.state == self.S2_COLLECT_DATA):
                # Button is being held down
                if self.isPressed == 1:
                    
                    self.ADC1.read_timed(self.buffy, self.adc_tim) # ISR callback
                    
                    ## Running count of the number of data points transmitted
                    self.data_sent = 0
            
                    ## Total number of points for transmittal
                    self.tot_data = len(self.buffy)               
                    
                    # Check that the step response is valid
                    for i in range(len(self.buffy)):
                        # Data is valid. Begin sending to UI front-end
                        if self.buffy[i] >= 4000:
                            print('START')
                            self.transitionTo(self.S3_SEND_DATA)
                            break
                        # Data is invalid.
                        else:
                            pass
                            
            elif(self.state == self.S3_SEND_DATA):
                # Sending the data through the UART
                print('{:f},{:f}'.format((1000/self.adc_tim.freq())*self.data_sent, self.buffy[self.data_sent]))     
                
                self.data_sent += 1
                
                # The transmission is terminated when allthe data is sent
                if self.data_sent >= self.tot_data:
                    print('END')
                    self.transitionTo(self.S1_WAIT_FOR_COMMAND)
                    
            # Error handling      
            else:
                pass
    
              
    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
        
        
    def callback(self, IRQ):
        ''' 
        @brief   An interupt callback function.
        @details An interrupt handler that is called when the User button is pressed.
                 Records the time in which the user button was pressed, in microseconds,
                 with respect to the start of the clock timer.
        @param IRQ The desired physical interupt line.
        '''
        self.isPressed = self.isPressed*(-1) + 1
        
        
if __name__ == '__main__':
    ## A UI_Back task object
    task = UI_Back()
    
    task.run() # Run the FSM