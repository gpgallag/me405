'''
@file GetChange.py

@brief Calculates the correct change for a given purchase in the fewest demoninations possible.

@details This class contains a single method which takes cost and payment of an 
         item containing the denominations being used for the purchase.
         The method then returns the change in the fewest denominations possible.

@author Grant Gallagher

@date January 13, 2021
'''

def getChange(price, payment):
    '''
    @brief      Computes change for monetary transaction.
    @details    This function takes cost and payment of an item containing the denominations being used for the purchase.
                The function then returns the change in the fewest denominations possible.
    @param price A 2-point decimal number representing the price of an item.
                 Ex. $99.99 == 99.99 
    @param payment A tuple containing the payment amount for an item containing the quantity of denominations. The payment
                   tuple contains pennies, nickles, dimes, quarters, dollars, fives, tens, and twenties in ascending order.
                   Ex. payment = (3, 0, 2, 1, 0, 0, 1) refers to 3 pennies, 2 quarters, 1 one-dollar bill, and 1 twenty-dollar bill.
    @return The correct change for the given purchase in the fewest denominations possible
            as an 8-object long tuple. If the input parameters are invalid, returns "None".
            If the price is "free" or there is no change to give back, returns a tuple full of zeros.
            Ex. change = (3, 0, 2, 1, 0, 0, 1) refers to 3 pennies, 2 quarters, 1 one-dollar bill, and 1 twenty-dollar bill.
    '''
    
    # Initialize variables
    ## A tuple representing the cent-equivalent values of the payment tuple.
    currency_key = (1, 5, 10, 25, 100, 500, 1000, 2000)
    
    ## A list of the number of dollar denominations.
    register   = [0, 0, 0, 0, 0, 0, 0, 0] # (ex: cents, nickles, dimes, quarters, dollars, fives, tens, twenties)
                                          
    ## The dollar-amount payment in cents as an integer value.
    payment_cents = 0
    
    ## The *temporary* dollar-amount change in cents as an integer value.
    register_cents = 0
    
    ## A tuple representing the number of dollar denomoninations in the change returned.
    change = ()
    
    
    # Check that price is a valid input
    while True:
        try:
            if (not ((type(price) == int) or (type(price) == float))) or (price < 0) or ((len(str(price).rsplit('.')[-1]) > 2) and (type(price) == float)):
                return
        except TypeError:
            return
        break    
    # Check that payment is a valid tuple
    if (type(payment) is not tuple) or (len(payment) != 8):
        return
    # Check that the denominations in payment are valid
    for denomination in payment:
        if (type(denomination) is not int) or (denomination < 0):
            return
        
        
    # Parse the payment to calculate the total dollar amount of payment in cents.
    for index in range(len(payment)):
        payment_cents += (payment[index] * currency_key[index]) 
    # Calculate the change in the register in cents.
    register_cents = int(payment_cents - price*100)
    # Check that the payment is sufficient for the price.
    if register_cents < 0:
        return
    
    
    # Check for trivial solutions (after valid inputs)
    if register_cents == 0:
        return (0, 0, 0, 0, 0, 0, 0, 0)
    
    
    # Sort the change into fewest equivalent denominations.
    for index in range(len(currency_key)):
        if register_cents >= currency_key[len(currency_key) - 1 - index]:
            register[len(register) - 1 - index] = int(register_cents / currency_key[len(currency_key) - 1 - index])
            register_cents %= currency_key[len(currency_key) - 1 - index]
            
    # Convert the register change (list) into the given change (tuple)
    for index in range(len(currency_key)):
        change += (register[index],)
    
    # Return the change as a tuple
    return(change)


# # Debugging Code
# if __name__ == '__main__':
#     print("Price Variance Tests")
#     price = 'String'
#     payment = (1, 1, 1, 1, 1, 1, 1, 1)
#     print("PriceType String: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = 'c'
#     print("PriceType Char: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = -1
#     print("PriceValue Negative: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = 1.001
#     print("PriceValue Three Decimal: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = 20357239584325731205
#     print("PriceValue Too Large: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = [0, 1, 2]
#     print("PriceType List: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = (1, 2, 's')
#     print("PriceType Tuple: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = 0
#     print("PriceValue Free: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = (21.53)
#     print("PriceType Valid: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = (1.07)
#     print("PriceType Valid: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
    
#     print()
    
#     print("Payment Varience Tests")
#     price = 12.37
#     payment = 'String'
#     print("PaymentType String: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     payment = 'c'
#     print("PaymentType Char: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     payment = 10
#     print("PaymentType Int: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     payment = (1, 1, 1, 1, 1, 1, 20)
#     print("PaymentLength 7-Long: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     payment = (1, 1, 1, 1, 1, 1, 1, 1, 20)
#     print("PaymentLength 9-Long: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     payment = (1, 1, 1, 0, 0, 0, 0, 0)
#     print("PaymentValue Insufficient: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = 35
#     payment = (0, 0, 0, 0, 0, 1, 1, 1)
#     print("PaymentValue Equal: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     payment = (0, 0, 0, 0, 0, 1, 1, 2)
#     print("PaymentValue Valid: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
#     price = 13.47
#     payment = (0, 1, 5, 7, 8, 1, 1, 1)
#     print("PaymentValue Valid: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))
    
#     print()
    
#     print("Math checking")
#     price = 235.36
#     payment = (7, 6, 3, 6, 12, 5, 1, 10)
#     print("PaymentValue Valid: getChange(" + str(price) + ", " + str(payment) + ") = " + str(getChange(price, payment)))

    
    