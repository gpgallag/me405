'''
@file vendotron.py

@brief This file contains a finite-state machine that simulates a vending machine.

@details This file contains a class encapsulates the finite-state machine, and relative member functions,
             used to simulate a vending machine - appropriately named 'Vendotron.'
             The vending machine accepts user inputs to simulate entering denominations
             of payments, select one of four drinks, and eject the balance. The vending
             machine then outputs strings of what the user did and what would have been
             performed - on real hardware - as feedback.

@package Lab0x01

@brief This package contains vendotron.py
       
@author Grant Gallagher

@date January 20, 2021
'''
import keyboard

class Vendotron:
    '''
    @brief This class  simulate a vending machine.
    @details This class encapsulates the finite-state machine, and relative member functions,
             used to simulate a vending machine - appropriately named 'Vendotron.'
             The vending machine accepts user inputs to simulate entering denominations
             of payments, select one of four drinks, and eject the balance. The vending
             machine then outputs strings of what the user did and what would have been
             performed - on real hardware - as feedback.
    '''
    
    ## State 0: Initialization
    S0_INIT = 0

    ## State 1: Wait for user input
    S1_WAIT = 1

    ## State 2: Return the change in balance
    S2_RETURN_CHANGE = 2

    ## State 3: Dispense a user-selected drink
    S3_RETURN_DRINK = 3

    
    def __init__(self):
        '''
        @brief Constructs a Vendotron task object.
        @details This initialization function sets the finite-state machine to state
                 0, unhooks all keyboard inputs, and declares multiple class variables
                 that hold drink prices, the user's balance, and other necessary 
                 quantities
        '''
        keyboard.unhook_all ()
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A list containing the cent-equivalent values of the user's payment [cent, nickle, dime, quarter, dollar, five, ten, twenty].
        self.payment = [0, 0, 0, 0, 0, 0, 0, 0]
        
        ## A list containing the price of selectable drinks, in cents [Cuke, Popsi, Spryte, Dr. Pupper]
        self.drinkPrices = [100, 120, 85, 110]

        ## A list of selectable drink keys
        self.drinkKeys = ['c', 'p', 's', 'd']
        
        ## A list of selectable drink names.
        self.drinkNames = ['Coke', 'Popsi', 'Spryte', 'Dr. Pupper']
        
        ## A list of various denominations of currency, set up in the same order as self.payment.
        self.currency = ['penny(s)', 'nickle(s)', 'dime(s)', 'quarter(s)', 'dollar(s)', 'five(s)', 'ten(s)', 'twenty(s)']
        
        ## A character that stores the user's input of drink choice. Ex: The input for "Dr. Pupper" is "p".
        self.drink = ""
        
        ## The user's key input.
        global pushedKey
        pushedKey = None
        
    def keystroke(self, thing):
        '''
        @brief Callback which runs when the user presses a key.
        '''
        global pushedKey
        pushedKey = thing.name
        
    def run(self):
        '''
        @brief Runs one iteration of the FSM task.
        @details The vendotron FSM can be in one of four various states: initiatization,
                 wait for user input, dispense a drink, or return change. Once called,
                 this function will continuously run a While loop which checks the state
                 of the FSM and responds appropriately. To exit the loop, the user
                 can press ctrl+c.
        '''
        
        keyboard.on_press (self.keystroke)
        
        while True:
            try:
                # Run State 0 Code    
                if(self.state == self.S0_INIT):
                    self.payment = [0, 0, 0, 0, 0, 0, 0, 0]
                    self.printWelcome()
                    self.state = self.S1_WAIT
            
                # Run State 1 Code
                elif(self.state == self.S1_WAIT):
                    self.checkInput(pushedKey)

                # Run State 2 Code
                elif(self.state == self.S2_RETURN_CHANGE): 
                    change = self.getChange(0,self.payment)
                    print("Returning your total balance of: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'...')
                    for index in range(len(change)):
                        if change[index] > 0:
                            print('Ejecting '+str(change[index])+' '+self.currency[index])
                            
                    print('\n------------------------------------------\n')
                    self.state = self.S0_INIT
                
                # Run State 3 Code
                elif(self.state == self.S3_RETURN_DRINK): 
                    if self.drink == "c":
                        print(self.payment)
                        self.payment = self.getChange(self.drinkPrices[0], self.payment)
                        print("Cuke has been dispensed.\n")
                        print(self.payment)
                    elif self.drink == "p":
                        self.payment = self.getChange(self.drinkPrices[1], self.payment)
                        print("Popsi has been dispensed.\n")
                    elif self.drink == "s":
                        self.payment = self.getChange(self.drinkPrices[2], self.payment)
                        print("Spryte has been dispensed.\n")
                    elif self.drink == "d":
                        self.payment = self.getChange(self.drinkPrices[3], self.payment)
                        print("Dr.Pupper has been dispensed.\n")
                    self.drink = ""
                    self.state = self.S2_RETURN_CHANGE
            
            except KeyboardInterrupt:
                keyboard.unhook_all()
                break
                    
    def checkInput(self, key):
        '''
        @brief   Checks the user's inputs.
        @details A class function used to interpret and filter user inputs. The user
                 is capable of selecting from one of 8 forms of denominations of currency:
                 pennies (0), nickles (1), dimes (2), quarters (3), dollars (4), fives (5),
                 tens (6), and twenties (7). The user is also allowed to select beverages from
                 one of the four beverages: Cuke (c), Popsi (p), Spryte (s), and Dr. Pupper (d).
                 Finally, the user can request for the change in remaining balance at any time
                 by pressing (e). Any other keystrokes will result in an invalid input message.
        '''
        global pushedKey
        if pushedKey:
            if pushedKey == "0":
                print ("You inserted a Penny.")
                self.payment[0] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')

            elif pushedKey == '1':
                print ("You inserted a Nickle.")
                self.payment[1] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')

            elif pushedKey == '2':
                print ("You inserted a Dime.")
                self.payment[2] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')
                
            elif pushedKey == '3':
                print ("You inserted a Quarter.")
                self.payment[3] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')
            
            elif pushedKey == '4':
                print ("You inserted a Dollar.")
                self.payment[4] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')
            
            elif pushedKey == '5':
                print ("You inserted a Five.")
                self.payment[5] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')
            
            elif pushedKey == '6':
                print ("You inserted a Ten.")
                self.payment[6] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')
            
            elif pushedKey == '7':
                print ("You inserted a Twenty.")
                self.payment[7] +=1
                print("Current balance: $"+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'\n')

            elif pushedKey == 'e' or pushedKey == 'E':
                print ("You pressed the eject button.\n")
                self.state = self.S2_RETURN_CHANGE
                
            elif pushedKey == 'c' or pushedKey == 'C':
                print ('You selected Cuke.')
                if self.getBalance(self.payment)>self.drinkPrices[0]:
                        self.drink = 'c'
                        self.state = self.S3_RETURN_DRINK
                else:
                    print('Insufficient Funds.\n'
                          'Cuke costs $'+str(format(self.drinkPrices[0]/100.0, '.2f'))+
                          ' and your current balance is $'+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'.\n')
                    pass
                
            elif pushedKey == 'p' or pushedKey == 'P':
                print ('You selected Popsi.')
                if self.getBalance(self.payment)>self.drinkPrices[1]:
                    self.drink = 'p'
                    self.state = self.S3_RETURN_DRINK
                else:
                    print('Insufficient Funds.\n'
                          'Popsi costs $'+str(format(self.drinkPrices[1]/100.0, '.2f'))+
                          ' and your current balance is $'+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'.\n')
                    pass
                
            elif pushedKey == 's' or pushedKey == 'S':
                print ('You selected Spryte.')
                if self.getBalance(self.payment)>self.drinkPrices[2]:
                    self.drink = 's'
                    self.state = self.S3_RETURN_DRINK
                else:
                    print('Insufficient Funds.\n'
                          'Spryte costs $'+str(format(self.drinkPrices[2]/100.0, '.2f'))+
                          ' and your current balance is $'+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'.\n')
                    pass
                
            elif pushedKey == 'd' or pushedKey == 'D':
                print ('You selected Dr. Pupper.')
                if self.getBalance(self.payment)>self.drinkPrices[3]:
                    self.drink = 'd'
                    self.state = self.S3_RETURN_DRINK
                else:
                    print('Insufficient Funds.\n'
                          'Dr. Pupper costs $'+str(format(self.drinkPrices[3]/100.0, '.2f'))+
                          ' and your current balance is $'+str(format(self.getBalance(self.payment)/100.0, '.2f'))+'.\n')
                    pass
            else:
                print('Sorry, that is not a valid input.\n')
            
                
            pushedKey = None
            
        
    def getChange(self, price, payment):
        '''
        @brief      Computes change for a monetary transaction.
        @details    This function takes cost and payment of an item containing the denominations being used for the purchase.
                    The function then returns the change in the fewest denominations possible.
        @param price A 2-point decimal number representing the price of an item.
                     Ex. $99.99 == 99.99 
        @param payment A tuple containing the payment amount for an item containing the quantity of denominations. The payment
                       tuple contains pennies, nickles, dimes, quarters, dollars, fives, tens, and twenties in ascending order.
                       Ex. payment = (3, 0, 2, 1, 0, 0, 1) refers to 3 pennies, 2 quarters, 1 one-dollar bill, and 1 twenty-dollar bill.
        @return The correct change for the given purchase in the fewest denominations possible
                as an 8-object long tuple. If the input parameters are invalid, returns "None".
                If the price is "free" or there is no change to give back, returns a tuple full of zeros.
                Ex. change = (3, 0, 2, 1, 0, 0, 1) refers to 3 pennies, 2 quarters, 1 one-dollar bill, and 1 twenty-dollar bill.
        '''
    
        # Initialize variables
        ## A tuple representing the cent-equivalent values of the payment tuple.
        currency_key = (1, 5, 10, 25, 100, 500, 1000, 2000)
        
        ## A list of the number of dollar denominations.
        register   = [0, 0, 0, 0, 0, 0, 0, 0] # (ex: cents, nickles, dimes, quarters, dollars, fives, tens, twenties)
                                              
        ## The dollar-amount payment in cents as an integer value.
        payment_cents = 0
        
        ## The *temporary* dollar-amount change in cents as an integer value.
        register_cents = 0
                
        # Check that price is a valid input
        while True:
            try:
                if (not ((type(price) == int) or (type(price) == float))) or (price < 0) or ((len(str(price).rsplit('.')[-1]) > 2) and (type(price) == float)):
                    return
            except TypeError:
                return
            break    
        # Check that payment is a valid tuple
        if (type(payment) is not list) or (len(payment) != 8):
            return
        # Check that the denominations in payment are valid
        for denomination in payment:
            if (type(denomination) is not int) or (denomination < 0):
                return
    
    
        # Parse the payment to calculate the total dollar amount of payment in cents.
        for index in range(len(payment)):
            payment_cents += (payment[index] * currency_key[index]) 
        # Calculate the change in the register in cents.
        register_cents = int(payment_cents - price)
        # Check that the payment is sufficient for the price.
        if register_cents < 0:
            return
                
                
        # Check for trivial solutions (after valid inputs)
        if register_cents == 0:
            return (0, 0, 0, 0, 0, 0, 0, 0)
                
                
        # Sort the change into fewest equivalent denominations.
        for index in range(len(currency_key)):
            if register_cents >= currency_key[len(currency_key) - 1 - index]:
                register[len(register) - 1 - index] = int(register_cents / currency_key[len(currency_key) - 1 - index])
                register_cents %= currency_key[len(currency_key) - 1 - index]
                            
        # Return the change as a list
        return register
    
    def getBalance(self, payment):
        '''
        @brief Calculates the user's balance.
        @details A function the converts the user's balance, which is stored as
                 a list of a quantity of denomincations of currency (ex. pennies, nickles...)
                 and returns a single integer representing the total balance in cents.
        @param payment A list that represents a collection of coins and bills. See self.payment.
        @return payment_cents An integer value that represents the user's balance in cents.
        '''
        ## A tuple representing the cent-equivalent values of the payment tuple.
        currency_key = (1, 5, 10, 25, 100, 500, 1000, 2000)
                                                      
        ## The dollar-amount payment in cents as an integer value.
        payment_cents = 0
        
        # Check that payment is a valid tuple
        if (type(payment) is not list) or (len(payment) != 8):
            return
        
        # Check that the denominations in payment are valid
        for denomination in payment:
            if (type(denomination) is not int) or (denomination < 0):
                return
            
        # Parse the payment to calculate the total dollar amount of payment in cents.
        for index in range(len(payment)):
            payment_cents += (payment[index] * currency_key[index]) 
        
        # Return the payment
        return payment_cents

    def printWelcome(self):
        '''
        @brief Prints a Vendotron^TM^ welcome message with beverage prices
        '''
        print('Thank you for choosing Vendotron!')
        print('Please insert coins, or select a beverage. \n')
        pass


    
    def clearDisplay():
        '''
        @brief Flushes the serial port of any chararacters.
        '''
        print("\x1B\x5B2J")
        print("\x1B\x5BH")
 
if __name__ == '__main__':
    ## A Vendotron class object.
    task1 = Vendotron()
    
    # Runs the vendotron FSM continuously.
    task1.run()   

