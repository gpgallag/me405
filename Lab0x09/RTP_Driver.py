'''
@file RTP_Driver.py

@brief This file contains a driver for a resistive touch panel.

@details The driver is composed of a single class that is used to initialize
         a resistive touch panel (RTP) object. The class contains methods used to
         scan the horizontal and vertical position of contact with the panel, detect
         whether there is any active contact with the panel, and two methods that scan
         for all of the components in one. Before use, the driver must be calibrated for
         the specified touch panel.

@package Lab0x09

@author Grant Gallagher

@date March 17, 2021
''' 
from pyb import ADC, Pin, micros, udelay, delay

class RTP_Driver:
    '''
    @brief    A driver class for a resistive touch panel.
    @details  This class contains an constructor used to define and calibrate an
              RTP object, methods to measure the raw horizontal and vertical contact readings,
              a method to indicate if there is contact with the RTP, and two methods that scan
              all components of the RTP.
    '''
    def __init__(self, x_p_pin, x_m_pin, y_p_pin, y_m_pin, x_len_cal, x_count_cal, y_len_cal, y_count_cal, center_count_cal):
        '''
        @brief The constructor for the RTP driver.
        @param x_p_pin    The Nucleo board pin name associated with the x_p lead on the resistive touch panel.
        @param x_m_pin    The Nucleo board pin name associated with the x_m lead on the resistive touch panel.
        @param y_p_pin    The Nucleo board pin name associated with the y_p lead on the resistive touch panel.
        @param y_m_pin    The Nucleo board pin name associated with the y_m lead on the resistive touch panel.
        @param x_len_cal  The calibration distance [mm] between two points along the horizontal axis, respective to x_count_cal.
        @param x_count_cal  The calibration count between two points along the horizontal axis, respective to x_len_cal.
        @param y_len_cal  The calibration distance [mm] between two points along the vertical axis, respective to y_count_cal.
        @param y_count_cal  The calibration count between two points along the vertical axis, respective to y_len_cal.
        @param center_count_cal The calibration count, located at the center of the platform along the RTP. (x_count, y_count).
        '''
        ## The pyb.Pin object associated with the x_p lead on the resistive touch panel.
        self.x_p = x_p_pin
        
        ## The pyb.Pin object associated with the x_m lead on the resistive touch panel.
        self.x_m = x_m_pin
        
        ## The pyb.Pin object associated with the y_p lead on the resistive touch panel.
        self.y_p = y_p_pin
        
        ## The pyb.Pin object associated with the y_m lead on the resistive touch panel.
        self.y_m = y_m_pin
        
        ## The pyb.ADC object associated with the x_m lead on the resistive touch panel, used to measure the Y component.
        self.ADC_x = ADC(self.x_m)
        
        ## The pyb.ADC object associated with the y_m lead on the resistive touch panel, used to measure the X component.
        self.ADC_y = ADC(self.y_m)
        
        ## A tuple representing the reference positions for the calibration of the x-component (min, max).
        self.x_len_cal = x_len_cal
        
        ## A tuple representing the reference counts for the calibration of the x-component (min, max).
        self.x_count_cal = x_count_cal
        
        ## A tuple representing the reference positions for the calibration of the y-component (min, max).
        self.y_len_cal = y_len_cal
        
        ## A tuple representing the reference counts for the calibration of the y-component (min, max).
        self.y_count_cal = y_count_cal
        
        ## A tuple representing the counts at the center of the RTP for the calibration of the center point (X, Y).
        self.center_count_cal = center_count_cal

        ## A placeholder, used to maximize the efficiency of the driver, that represents the last RTP component that was scanned.
        self.last_scan = ''
        
        self.x_position = 0
        
        self.y_position = 0
        
        self.x_position_old = 0
        
        self.y_position_old = 0
        
        self.delta_x = 0
        
        self.delta_y = 0
        
        
    def scan_x(self):
        '''
        @brief   Scans the X component of the RTP.
        @details Scans the X position along the resistive touch panel by energizing
                 the resistor divider between x_p and x_m, floating y_p, and measuring the
                 ADC at y_m. Then, the count is converted to distance (mm) from the center point.
        @return x_position A float representing the horizontal displacement (mm) of the
                           point of contact from the center of the touch panel.
        '''
        # Checks if the last component scanned was 'Z' (more efficient)
        if self.last_scan == 'z':
            self.x_p.init(mode=Pin.OUT_PP, value=1)
            # No change to x_m
            self.y_p.init(mode=Pin.IN)
            # No change to y_m
        
        # Must (re)initialize each pin
        else:
            self.x_p.init(mode=Pin.OUT_PP, value=1)
            self.x_m.init(mode=Pin.OUT_PP, value=0)
            self.y_p.init(mode=Pin.IN)
            self.ADC_y = ADC(self.y_m)
        
        self.last_scan = 'x' # Indicate that this was the last component scanned
        
        udelay(4) # Wait for signal to settle
        
        ## A float representing the vertical displacement (mm) of the point
        #  of contact from the center of the touch panel.
        x_position = (self.center_count_cal[0]-self.ADC_y.read())*(self.x_len_cal[1]-self.x_len_cal[0])/(self.x_count_cal[1]-self.x_count_cal[0])
        
        return x_position
    
    def scan_y(self):
        '''
        @brief   Scans the Y component of the RTP.
        @details Scans the Y position along the resistive touch panel by energizing
                 the resistor divider between y_p and y_m, floating x_p, and measuring the
                 ADC at x_m. Then, the count is converted to distance (mm) from the center point.
        @return y_position A float representing the vertical displacement (mm) of the
                           point of contact from the center of the touch panel.
        '''
        # Checks if the last component scanned was 'Z' (more efficient)
        if self.last_scan == 'z':
            # No change to x_p
            self.ADC_x = ADC(self.x_m)
            # No change to y_p
            self.y_m.init(mode=Pin.OUT_PP, value=1)
         
        # Must (re)initialize each pin
        else:
            self.x_p.init(mode=Pin.IN)
            self.ADC_x = ADC(self.x_m)
            self.y_p.init(mode=Pin.OUT_PP, value=1)
            self.y_m.init(mode=Pin.OUT_PP, value=0)
        
        
        self.last_scan = 'y' # Indicate that this was the last component scanned
        
        udelay(4) # Wait for signal to settle
        
        ## A float representing the vertical displacement (mm) of the point
        #  of contact from the center of the touch panel.
        y_position = (self.center_count_cal[1]-self.ADC_x.read())*(self.y_len_cal[1]-self.y_len_cal[0])/(self.y_count_cal[1]-self.y_count_cal[0])
        
        return y_position
    
    def scan_z(self):
        '''
        @brief   Scans the Z component of the RTP.
        @details Scans the resistive touch panel to detect if there is contact with the screen by
                 energizing the resistor divider between y_p and x_m, floating x_p, and measuring the
                 ADC at y_m. A count measurement of 4000 or more indicates that there is no contact.
        @return contact A boolean value representing whether there is contact with the RTP.
        '''
        # Checks if the last component scanned was 'X' (more efficient)
        if self.last_scan == 'x':
            self.x_p.init(mode=Pin.IN)
            # No change to x_m
            self.y_p.init(mode=Pin.OUT_PP, value=1)
            # No change to y_m
        
        # Checks if the last component scanned was 'Y' (more efficient)
        elif self.last_scan == 'y':
            # No change to x_p
            self.x_m.init(mode=Pin.OUT_PP, value=0)
            # No change to y_p
            self.ADC_y = ADC(self.y_m)
        
        # Must (re)initialize each pin
        else:
            self.x_p.init(mode=Pin.IN)
            self.x_m.init(mode=Pin.OUT_PP, value=0)
            self.y_p.init(mode=Pin.OUT_PP, value=1)
            self.ADC_y = ADC(self.y_m)
        
        self.last_scan = 'z'# Indicate that this was the last component scanned
                
        udelay(4) # Wait for signal to settle
        
        ## A boolean value representing whether there is contact with the RTP.
        contact = self.ADC_y.read() < 4000
        
        return contact
    
    def scan_rtp(self):
        '''
        @brief Scans the X, Y, and Z components of the RTP
        @details Scans the horizontal and vertical displacement (mm) from the center point
                 of the resistive touch panel, as well as whether there is contact with the panel.
                 If there is no contact with the touch panel, then returns "None" for both displacements.
        @return A tuple representing the x-position (mm), y-position (mm), and state of contact (bool) with the RTP.
        '''
        # Measure and record if there is contact with the RTP.
        contact = self.scan_z()
        
        # Contact detected
        if contact == True:
            # Measure and record the horizontal/vertical displacements
            x_position = self.scan_x()
            y_position = self.scan_y()
            return (x_position, y_position, contact)
        
        # No contact detected, state of system is known (save time)
        else:
            return (None, None, False)
        
    def scan_rtp_fast(self):
        '''
        @brief See scan_rtp().
        @details An optimized version of scan_rtp() that scans the X, Y, and Z components
                 of the touch panel in a more efficient manner by removing calls to other class
                 methds, minimimizing the number of pin configurations and scanning the
                 components in a better sequence.
        @return A tuple representing the x-position (mm), y-position (mm), and state of contact (bool) with the RTP.
        '''
        
        # (re)Initialize every pin to scan for 'X' component
        self.x_p.init(mode=Pin.OUT_PP, value=1)
        self.x_m.init(mode=Pin.OUT_PP, value=0)
        self.y_p.init(mode=Pin.IN)
        self.ADC_y = ADC(self.y_m)
        
        
        self.x_position_old = self.x_position
        # Measure and record the horizontal position (mm)
        self.x_position = (self.center_count_cal[0]-self.ADC_y.read())*(self.x_len_cal[1]-self.x_len_cal[0])/(self.x_count_cal[1]-self.x_count_cal[0])
        
        # Initialize pins to scan for 'Z' component
        self.x_p.init(mode=Pin.IN)
        self.y_p.init(mode=Pin.OUT_PP, value=1)
        
        # Scan for contact with the RTP.
        contact = self.ADC_y.read() < 4000
        
        # No contact detected, state of system is known (save time)
        if contact == False:
            return (0, 0, False)
        
        # Contact detected
        else:
            # Initialize pins to scan for 'Y' component
            self.ADC_x = ADC(self.x_m)
            self.y_m.init(mode=Pin.OUT_PP, value=0)
            
            self.y_position_old = self.y_position
            # Measure and record the vertical position (mm)
            self.y_position = (self.center_count_cal[1]-self.ADC_x.read())*(self.y_len_cal[1]-self.y_len_cal[0])/(self.y_count_cal[1]-self.y_count_cal[0])
            
            # Return that measured components
            return (self.x_position, self.y_position, contact)
        

if __name__ == '__main__':   
    
    # Initialize Pin Objects
    PA0 = Pin.board.PA0
    PA1 = Pin.board.PA1
    PA6 = Pin.board.PA6
    PA7 = Pin.board.PA7
    
    #  RTP dimensions
    #  (Because this method is not calibrated for the screen itself, please
    #  use RTP_Driver_Calibrated instead. Must use counts instead of measured
    #  dimensions for best results.)
    x_len_cal        = (-20,  20)
    x_count_cal      = (1636, 2452)
    y_len_cal        = (-20,  20)
    y_count_cal      = (1291, 2585)
    center_count_cal = (2046, 2035) 
    
    ## RTP_Driver object.
    task = RTP_Driver(PA0, PA1, PA6, PA7, x_len_cal, x_count_cal, y_len_cal, y_count_cal, center_count_cal)
    
    # Continuously print position
    while True:
        # Scan for components and get rough estimate of latency
        start = micros()
        data = task.scan_rtp_fast()
        end = micros()
        
        # Print positional components
        if data[2] == True:
            print('{:.1f},{:.1f}'.format(data[0], data[1]))
        else:
            print('No contact detected.')
            
        # Print latency (approx.)
        print('Scan Time (approx.): '+ str(start-end)+'\n')
        
        # Delay (saves processing power)
        delay(100)
 

    # - - - - This section is used to record the data for calibration - - - - 
    # with open ('Touchpad_Data.csv', 'w') as file:   
    #     while True:
    #         data = task.read_rtp()
    #         print(data)
    #         if data[2] == False:
    #             pass
    #         else:
    #             x_position = data[0]
    #             y_position = data[1]
    #             file.write('{:},{:}\n'.format(x_position, y_position))
            
    #         delay(100)