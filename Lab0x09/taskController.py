'''
@file taskController.py

@brief  This file contains finite-state machine class that schedules control tasks.

@details This class contains a finite-state machine that is used to schedule
         the operation of various state feedback tasks to control a 2DoF ball
         balancing system. The FSM runs on a specified interval that repetitively
         calls for various system measurement/actuation tasks to control the MIMO system.

@package Lab0x09

@author Grant Gallagher
@author Mitchell Carroll

@date March 17, 2021
'''
import pyb
from pid import PID

class TaskController:
    '''
    @brief Encapsulates the operation of a full state feedback controller.
    
    @details This class contains a finite-state machine that is used to schedule
             the operation of various state feedback tasks to control a 2DoF ball
             balancing system. The FSM runs on a specified interval that repetitively
             calls for tasks which measure encoder values, resisitive touch panel values,
             IMU values, and sets the PWM level of motors.
    '''
    ## State 0: Initialization.
    S0_INIT = 0
    
    ## STATE 1: Calibration.
    S1_CALIBRATION = 1
    
    ## STATE 2: Control (Platform).
    S2_STANDBY = 2
    
    ## STATE 3: Control (Platform + Ball).
    S3_CONTROL = 3

    ## Length of the platform [m].
    l_p = 110e-3
    
    ## Length of the motor arm [m].
    r_m = 60e-3

    def __init__(self, moe1, moe2, enc1, enc2, controller1, controller2, RTP, IMU, interval, debug=False):
        '''
        @brief Constructs a TaskController object. 
        @param moe1        A MotorDriver object used to control the output functions of Motor 1.
        @param moe2        A MotorDriver object used to control the output functions of Motor 2.
        @param enc1        An EncoderDriver object used to measure the count and position of Encoder 1.
        @param enc2        An EncoderDriver object used to measure the count and position of Encoder 2.
        @param controller1 A ClosedLoop controller object used to calculate the PWM level of Motor 1 with full-state feedback.
        @param controller2 A ClosedLoop controller object used to calculate the PWM level of Motor 2 with full-state feedback.
        @param RTP         An RTP_Driver object used to measure the contact point on the resistive touch panel.
        @param IMU         An IMU object used to measure the orientation of the platform during calibration.
        @param interval    An integer representing the time, in ms, between iterations of the TaskController FSM.
        @param debug       A boolean value that determines debugging state.
        '''                     
        ## A MotorDriver object used to control the output functions of Motor 1.
        self.moe1 = moe1
        
        ## A MotorDriver object used to control the output functions of Motor 2.
        self.moe2 = moe2
        
        ## An EncoderDriver object used to control the output functions of Encoder 1.
        self.enc1 = enc1
        
        ## An EncoderDriver object used to control the output functions of Encoder 2.
        self.enc2 = enc2
        
        ## An RTP_Driver object used to measure the contact point on the resistive touch panel.
        self.RTP = RTP
        
        ## A BNO055 object used to measure the orientation of the IMU.
        self.IMU = IMU       
        
        ## A ClosedLoop object used to implement state feedback control for Motor/Encoder 1.
        self.controller1 = controller1
        
        ## A ClosedLoop object used to implement state feedback control for Motor/Encoder 2.
        self.controller2 = controller2
        
        ## An integer representing the amount of time in milliseconds between runs of the task. 
        self.interval = interval    
        
        ## A boolean value representing if Encoder 1 has been calibrated.
        self.enc1_calibrated = False
        
        ## A boolean value representing if Encoder 2 has been calibrated.
        self.enc2_calibrated = False
    
        ## A float representing the horizontal displacement of the ball from the center of the touch panel, in meters.
        self.x = 0
        
        ## A float representing vertical displacement of the ball from the center of the touch panel, in meters.
        self.y = 0
        
        ## A float representing the horizontal velocity of the ball with respect to the touch panel, in m/s.
        self.x_dot = 0
        
        ## A float representing the vertical velocity of the ball with respect to the touch panel, in m/s.
        self.y_dot = 0
        
        ## The current state of the task controller FSM.
        self.state = 0
        
        ## The debug state (boolean).
        self.debug = debug
        
    def run(self):
        '''
        @brief Runs one iteration of the FSM task.
        @details The TaskController FSM can be one of four various states: Initialization,
                 Calibration, Control of the Platform, or Control of the Ball. Upon powering up,
                 the system immediately enters S0 - Initiailization - where a small delay is applied
                 and various system parameters are initialized. After one iteration, the FSM moved into
                 a calibration state, which zeros each encoders at a level position determined by the IMU. Once
                 both encoders are calibration, the system can either be in control of the platform, or the platform
                 and ball together- depending on whether the RTP senses contact.
        '''
        ## An integer representing the current time in milliseconds since epoch.
        self.current_time = pyb.millis()
                    
        ## The "timestamp" for when the task should run next.
        self.next_time = self.current_time + self.interval
        
        try:
            # Run forever (FSM)
            with open ('data.csv', 'w') as file:  
                while True:
                    self.current_time = pyb.millis() # Update the current time
                    # Check to see if the time exceeded the interval
                    if (self.current_time >= self.next_time):  
                        
                        if(self.state == self.S0_INIT):
                            print('Ready.\n\n')
                            self.moe1.enable() # Enable both motors.
                            
                            
                            pyb.delay(1000) # Must include a brief delay to compensate for IMU initialization time.
                            
                            ## A float representing the angular position of the platform about the x-axis [rad].
                            self.theta_x = 0
                            
                            ## A float representing the angular velocity of the platform about the x-axis [rad/s].
                            self.theta_dot_x = 0
                            
                            ## A float representing the angular position of the platform about the y-axis [rad].
                            self.theta_y = 0
                            
                            ## A float representing the angular velocity of the platform about the y-axis [rad/s].
                            self.theta_dot_y = 0
                            
                            ## A float representing the duty level to be applied to the motor controlling the x-axis of the platform.
                            self.T_x = 0
                            
                            ## A float representing the duty level to be applied to the motor controlling the y-axis of the platform.
                            self.T_y = 0
                            
                            ## a float representing the last-measured horizontal (x) displacement of the ball with respect to the platform [m].
                            self.x_old = 0
                            
                            ## a float representing the last-measured vertical (y) displacement of the ball with respect to the platform [m].
                            self.y_old = 0
                            
                            ## A boolean value representing the state of contact with the resistive touch panel.
                            z = 0
                            
                            ## The current state of the FSM.
                            self.state = 1
                            
                            ## A PID controller used to position the platform (motor 1) while calibrating the encoders.
                            self.pid1= PID(3.5, 0.08, 0.05)
                            
                            ## A PID controller used to position the platform (motor 2) while calibrating the encoders.
                            self.pid2= PID(3.5, 0.08, 0.05)
                        
                        # State 1
                        elif(self.state == self.S1_CALIBRATION):
                            euler = self.IMU.euler()
                            self.enc1.update()
                            self.enc2.update()                    
                            
                            if abs(euler[1]) <= 0.0625 and  self.enc1_calibrated == False:
                                print('Encoder 1 has been calibrated.')
                                self.enc1.set_position(0)
                                self.enc1_calibrated = True
                            
                            if abs(euler[2]) <= 0.0625 and self.enc2_calibrated == False:
                                print('Encoder 2 has been calibrated.')
                                self.enc2.set_position(0)
                                self.enc2_calibrated = True
                                
                            if self.enc1_calibrated and self.enc2_calibrated:
                                print('Both encoders have been calibrated. You may now place the ball on the platform.')
                                self.state = 2
                            
                            T_x = self.pid2.update(0, euler[1])
                            T_y = self.pid1.update(0, euler[2])
                            
                            self.moe1.set_duty(T_y)
                            self.moe2.set_duty(T_x)
                            
                            if self.debug:
                                print('Euler Angles: '+ '({:.3f}, {:.3f}, {:.3f})'.format(euler[0], euler[1], euler[2])+ ' | Tx: {:.2f} | Ty: {:.2f}'.format(T_x, T_y))
                                
                        # State 2   
                        elif(self.state == self.S2_STANDBY):               
                            z = self.RTP.scan_z()
                            
                            # Update encoders
                            self.enc1.update()
                            self.enc2.update()
                            
                            # Calculate the angular position of the platform, in radians
                            self.theta_x = (-1)*(self.enc2.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                            self.theta_y = (-1)*(self.enc1.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                                           
                            # Calculate the angular velocity of the platform, in radians/s
                            self.theta_dot_x = (-1)*(self.enc2.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            self.theta_dot_y = (-1)*(self.enc1.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            
                            # Calculate the required duty levels for the motors
                            self.T_x = self.controller1.update(0, self.theta_dot_x, 0, self.theta_x)
                            self.T_y = self.controller2.update(0, self.theta_dot_y, 0, self.theta_y) 
            
                            # Set the duty levels of the motors
                            self.moe1.set_duty(self.T_y)
                            self.moe2.set_duty(self.T_x)
                            
                            # Contact detected - transition to State 3
                            if z == True:           
                                self.x, self.y, z = self.RTP.scan_rtp_fast()
                                
                                # Convert RTP readings from mm to m
                                self.x = self.x/1000
                                self.y = self.y/1000
                                
                                self.state = 3
                                continue
                            if self.debug:
                                print('No ball detected')
                            
                            
                        # State 3: Control
                        elif(self.state == self.S3_CONTROL):
                           
                            # Update old and current positions
                            self.x_old = self.x
                            self.y_old = self.y
                            self.x, self.y, z = self.RTP.scan_rtp_fast()
                            
                            # No contact detected - immediately transition to State 2
                            if z == False:
                                self.state = 2
                                continue
                            
                            # Update encoders
                            self.enc1.update()
                            self.enc2.update()
                            
                            # Convert RTP readings from mm to m
                            self.x = self.x/1000
                            self.y = self.y/1000
                            
                            # Calculate the velocity of the ball with respect to the platform, in m/s
                            self.x_dot = (self.x - self.x_old) * 1000 / self.interval
                            self.y_dot = (self.y - self.y_old) * 1000 / self.interval
                            
                            # Calculate the angular position of the platform, in radians
                            self.theta_x = (-1)*(self.enc2.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                            self.theta_y = (-1)*(self.enc1.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                                    
                            # Calculate the angular velocity of the platform, in radians/s
                            self.theta_dot_x = (-1)*(self.enc2.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            self.theta_dot_y = (-1)*(self.enc1.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            
                            # Calculate the required duty levels for the motors
                            self.T_x = self.controller1.update(self.y_dot, self.theta_dot_x, self.y, self.theta_x)
                            self.T_y = self.controller2.update(self.x_dot, self.theta_dot_y, self.x, self.theta_y)              
                            
                            # Set the duty levels of the motors
                            self.moe1.set_duty(self.T_y)
                            self.moe2.set_duty(self.T_x)
                            file.write('{:},{:},{:},{:}\n'.format(self.x_dot, self.theta_dot_x, self.x, self.theta_x))
                            if self.debug:
                                print('Ball Detected | Tx: {:.2f} | Ty: {:.2f}'.format(self.T_x, self.T_y))
                        
                        # Error handling
                        else:
                            pass
                        
                        self.next_time = self.current_time + self.interval # Update the "scheduled" timestamp
         
        # Turn both of the motors off and exit the program.
        except:
              self.moe1.set_duty(0)
              self.moe2.set_duty(0)
              self.moe1.disable()
              print('\n\nEXITING PROGRAM AND TURNING MOTORS OFF\n\n')
        
                