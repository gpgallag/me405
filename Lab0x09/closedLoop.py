'''
@file  closedLoop.py

@brief  This file contains a closed-loop state feedback controller class.

@details  This class is used to calculate one iteration of closed-loop state feeedback control.
The controller utilizes proportional and partial integral control for a MISO system. To implement closed-loop control
appropriatle, the method update() should be called at regular intervals, along with updated encoder
velocities.

@package Lab0x09

@author Grant Gallagher
@author Mitchell Carroll

@date March 17, 2021
'''
class ClosedLoop:
    '''
    @brief      Utilize full state feedback control to calculate the necessary PWM level output to apply to a BDC motor.
    
    @details    This class utilizes full-state feedback control, with partial integral control,
                to apply accurate and responsive correction to the motor position of an unstable system.
                The main function of this class, update(), runs one iteration
                to determine the error in encoder displacement/velocity, and RTP contact displacement/velocity
                to apply a corrective PWM duty level to a BDC motor. Note: each motor/encoder *must* have its own
                ClosedLoop controller object if using partial integral control.
    '''
    def __init__(self, k_m, k_ctrl):
        '''
        @brief Constructs a ClosedLoop object.
        @param k_m A float representing the motor gain.
        @param k_ctrl A 4x1 array (list) containing the control system gains used for full-state feedback control. [N*s, N*m*s, N, N*m]
        '''           
        ## A float representing the motor gain.
        self.k_m = k_m
        
        ## A 4x1 array (list) containing the control system gains used for full-state feedback control. [N*s, N*m*s, N, N*m]
        self.k_ctrl = k_ctrl
                
        ## A float representing the corrective PWM level from *partial* integral control.
        self.integral = 0
        
        
    def update(self, vel_ball, vel_platform, pos_ball, pos_platform):
        ''' 
        @brief              Calculate the PWM level to send to a motor to apply a corrective torque to the system.
        @param vel_ball     A float representing the velocity of the ball with respect to the platform [m/s].
        @param vel_platform A float representing the velocity of the platform [rad/s].
        @param pos_ball     A float representing the displacement of the ball with respect to the center of the platform [m].
        @param pos_platform A float representing the displacement of the platform from equillibrium [rad].
        @return level       A float representing the necessary duty level to be applied to the motor.
        '''
        
        # Calculate the corrective level obtained from integrating the error in the ball postiion/velocity.
        self.integral += (0.5*pos_ball + 0.01*pos_platform)*self.k_m
        
        ## The total duty level applied to the motor.
        level = (-1)*(self.k_ctrl[0]*vel_ball + self.k_ctrl[1]*vel_platform + self.k_ctrl[2]*pos_ball + self.k_ctrl[3]*pos_platform) * self.k_m + self.integral + 30
        return level # Returns the level

        