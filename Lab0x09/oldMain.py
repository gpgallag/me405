'''
@file main.py

@brief Controller
'''
# %% Import Libraries

import pyb
import machine

from encoderDriver import EncoderDriver
from motorDriver import MotorDriver
from RTP_Driver import RTP_Driver
from bno055 import BNO055
from closedLoop import ClosedLoop

# %% Initialize motor objects
print('Initializing motors')

## Motor nSLEEP pin object (used to enable the driver chip), initialized to PA5
pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
## Motor nFAULT pin object (used for fault detection), initialized to the blue user button.
pin_nFAULT = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)
## A timer object using timer 3 with a frequency of 20kHz.
tim3 = pyb.Timer(3, freq=20000)


## A dictionary item containing the pin address and channel number for the M1- output pin.
M1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B4),
          'Channel': 1 }
## A dictionary item containing the pin address and channel number for the M1+ output pin.
M1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B5),
          'Channel': 2 }
## A MotorDriver object used to control the output functions of Motor 1.
moe1 = MotorDriver(pin_nSLEEP, pin_nFAULT, M1_CH1, M1_CH2, tim3)


## A dictionary item containing the pin address and channel number for the M2- output pin.
M2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B0),
          'Channel': 3 }
## A dictionary item containing the pin address and channel number for the M2+ output pin.
M2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B1),
          'Channel': 4 }
## A MotorDriver object used to control the output functions of Motor 2.
moe2 = MotorDriver(pin_nSLEEP, pin_nFAULT, M2_CH1, M2_CH2, tim3)

# Enable BOTH motors.
moe1.enable()
moe2.enable()

# %% Initialize encoder objects
print('Initializing encoders')
## A dictionary item containing the pin address and channel number for the E1_CH1 input pin.
E1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B6),
          'Channel': 1 }
## A dictionary item containing the pin address and channel number for the E1_CH2 input pin.
E1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B7),
          'Channel': 2 }
## A timer object using timer 4 with a period of 65535.
tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF)
## An EncoderDriver object used to measure the count and position of Encoder 1.
enc1 = EncoderDriver(E1_CH1, E1_CH2, tim4, debug=False)


## A dictionary item containing the pin address and channel number for the E2_CH1 input pin.
E2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.C6),
          'Channel': 1 }
## A dictionary item containing the pin address and channel number for the E2_CH2 input pin.
E2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.C7),
          'Channel': 2 }
## A timer object using timer 8 with a period of 65535.
tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF)
## An EncoderDriver object used to measure the count and position of Encoder 1.
enc2 = EncoderDriver(E2_CH1, E2_CH2, tim8, debug=False)

# %% Initialize the resistive touch panel object
print('Initializing touch panel')
## The Nucleo board pin name associated with the x_p lead on the resistive touch panel.
PA0 = pyb.Pin.board.PA0
## The Nucleo board pin name associated with the x_m lead on the resistive touch panel.
PA1 = pyb.Pin.board.PA1
## The Nucleo board pin name associated with the y_p lead on the resistive touch panel.
PA6 = pyb.Pin.board.PA6
## The Nucleo board pin name associated with the y_m lead on the resistive touch panel.
PA7 = pyb.Pin.board.PA7

## The calibration distance [mm] between two points along the horizontal axis, respective to x_count_cal.
x_len_cal        = (-20,  20)
## The calibration count between two points along the horizontal axis, respective to x_len_cal.
x_count_cal      = (1636, 2452)
## The calibration distance [mm] between two points along the vertical axis, respective to y_count_cal.
y_len_cal        = (-20,  20)
## y_count_cal  The calibration count between two points along the vertical axis, respective to y_len_cal.
y_count_cal      = (1291, 2585)
## The calibration count, located at the center of the platform along the RTP. (x_count, y_count).
center_count_cal = (2046, 2035) 

## RTP_Driver object.
RTP = RTP_Driver(PA0, PA1, PA6, PA7, x_len_cal, x_count_cal, y_len_cal, y_count_cal, center_count_cal)

# %% Initialize IMU object
print('Initializing IMU')

## Pyboard hardware I2C.
i2c = machine.I2C(1)

## An IMU object used to measure the orientation of the platform during calibration
imu = BNO055(i2c)

# %% States

## State 0: Initialization
S0_INIT = 0

## STATE 1: Calibration
S1_CALIBRATION = 1

## STATE 2: Standby
S2_STANDBY = 2

## STATE 3: Control
S3_CONTROL = 3
       
# %% Controller Attributes
print('Definining controller attributes')

k1 = -15
k2 = -0.4
k3 = -50
k4 = -6

k_m = 2.21*1000*2/(12*13.8)

k_ctrl = (k1, k2, k3, k4)

controller = ClosedLoop(k_m, k_ctrl)

l_p = 110e-3  ## Length of the platform [m]
r_m = 60e-3   ## Length of the motor arm [m]

##  The amount of time in milliseconds between runs of the task.
interval = 12

start_time = pyb.millis()

## The timestamp for the first iteration.
current_time = pyb.millis()
        
## The "timestamp" for when the task should run next.
next_time = current_time + interval

enc1_calibrated = False
enc2_calibrated = False

gravity = imu.gravity()

x = 0

y = 0

pyb.delay(1000)

# %% Main controller Finite-state Machine
print('Ready.\n\n')

moe2.enable()

old_start_task = 0
start_task = 0
try:
    # Run forever (FSM)
    while True:
        ## The timestamp for the first iteration.
        current_time = pyb.millis()
        
        if (current_time >= next_time):  
            if(state == S1_CALIBRATION):
                gravity = imu.gravity()
                print(gravity)
                enc1.update()
                enc2.update()
                
                if abs(gravity[1]) < 0.01 and  enc1_calibrated == False:
                    print('ENC1 DONE')
                    enc1.set_position(0)
                    enc1_calibrated = True
                
                if abs(gravity[0]) < 0.01 and enc2_calibrated == False:
                    print('ENC2 DONE')
                    enc2.set_position(0)
                    enc2_calibrated = True
                    
                if enc1_calibrated and enc2_calibrated:
                    print('DONE')
                    state = 2
                    
            elif(state == S2_STANDBY):
                old_start_task = start_task
                start_task = pyb.micros()
                print('STATE 2 - - - - - - Latency: ' + str(start_task - old_start_task))
                
                # Get old positions
                x_old = x
                y_old = y
                
                # Get current positions
                x, y, z = RTP.scan_rtp_fast()
                
                # Update encoders
                enc1.update()
                enc2.update()
                
                
                
                theta_x = (-1)*(enc2.get_position() * 2 * 3.14159 / 4000) * l_p / r_m
                theta_y = (-1)*(enc1.get_position() * 2 * 3.14159 / 4000) * l_p / r_m
                               
                theta_dot_x = (-1)*(enc2.get_delta() * 1000 * 2 * 3.14159 / (interval * 4000)) * l_p / r_m
                theta_dot_y = (-1)*(enc1.get_delta() * 1000 * 2 * 3.14159 / (interval * 4000)) * l_p / r_m
                
                T_x = (-1)*(k2*theta_dot_x + k4*theta_x)*k_m
                T_y = (-1)*(k2*theta_dot_y + k4*theta_y)*k_m

                moe1.set_duty(int(T_y))
                moe2.set_duty(int(T_x))
                
                if z == True:
                    x, y, z = RTP.scan_rtp_fast()
                    x = x/1000
                    y = y/1000
                    state = 3
                    continue
                
            elif(state == S3_CONTROL):
                old_start_task = start_task
                start_task = pyb.micros()
                print('STATE 3 - - - - - - Latency: ' + str(start_task - old_start_task))
                
                x_old = x
                y_old = y
                
                x, y, z = RTP.scan_rtp_fast()
                
                # Move immediately to state 2, RTP readings are bad.
                if z == False:
                    state = 2
                    continue
                
                x = x/1000
                y = y/1000
                
                # print('old: ' + str(x_old) + '  ' + str(y_old))
                # print('current:                          ' + str(x) + '  ' + str(y))
                x_dot = (x - x_old) * 1000 / interval
                y_dot = (y - y_old) * 1000 / interval
                
                # print('Ball velocities:                                   '+str(x_dot) + '  ' + str(y_dot)  )
                enc1.update()
                enc2.update()
                
                theta_x = (-1)*(enc2.get_position() * 2 * 3.14159 / 4000) * l_p / r_m
                theta_y = (-1)*(enc1.get_position() * 2 * 3.14159 / 4000) * l_p / r_m
                               
                theta_dot_x = (-1)*(enc2.get_delta() * 1000 * 2 * 3.14159 / (interval * 4000)) * l_p / r_m
                theta_dot_y = (-1)*(enc1.get_delta() * 1000 * 2 * 3.14159 / (interval * 4000)) * l_p / r_m
                
                
                
                T_x = controller.update(y_dot, theta_dot_x, y, theta_x)
                T_y = controller.update(x_dot, theta_dot_y, x, theta_y)              
                
                moe1.set_duty(int(T_y))
                moe2.set_duty(int(T_x))
            
            # Error handling
            else:
                pass
            
            next_time = current_time + interval # Update the "Scheduled" 
            # print(pyb.millis() - start_time)
except:
    moe1.set_duty(0)
    moe2.set_duty(0)
    print('\n\nEXITING PROGRAM AND TURNING MOTORS OFF\n\n')

