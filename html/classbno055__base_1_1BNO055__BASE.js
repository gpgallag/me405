var classbno055__base_1_1BNO055__BASE =
[
    [ "__init__", "classbno055__base_1_1BNO055__BASE.html#a94d64e540e25d0fe5a1ef913f98e9fc8", null ],
    [ "cal_status", "classbno055__base_1_1BNO055__BASE.html#ab09153d6e6f484bec02cc319359b07ce", null ],
    [ "calibrated", "classbno055__base_1_1BNO055__BASE.html#ada9cd2ec35752625bd078f6a4f62e7f0", null ],
    [ "external_crystal", "classbno055__base_1_1BNO055__BASE.html#ae1d11378c82474df3eb495df0301e6ab", null ],
    [ "mode", "classbno055__base_1_1BNO055__BASE.html#aeac6aa198960501aee685503a54958ac", null ],
    [ "reset", "classbno055__base_1_1BNO055__BASE.html#a7b14453c7a3e6d3143632807bf689bd8", null ],
    [ "scaled_tuple", "classbno055__base_1_1BNO055__BASE.html#a32a206260a80f06e3987d7f5ed06a3eb", null ],
    [ "temperature", "classbno055__base_1_1BNO055__BASE.html#a81e86ebd810d03e17bab2acc9b16c79f", null ],
    [ "accel", "classbno055__base_1_1BNO055__BASE.html#a64c743b576e25c59c191a211bea34d1f", null ],
    [ "address", "classbno055__base_1_1BNO055__BASE.html#a73bd243dbd5adc1ca6ad6f43cfd1fd7f", null ],
    [ "crystal", "classbno055__base_1_1BNO055__BASE.html#acacda62fd7d86de1c769dc18c03d0ef8", null ],
    [ "euler", "classbno055__base_1_1BNO055__BASE.html#a95593b349a7d210205cf9f1cd7481ba5", null ],
    [ "gravity", "classbno055__base_1_1BNO055__BASE.html#a78882f6f70d74ddee25b5050c505af88", null ],
    [ "gyro", "classbno055__base_1_1BNO055__BASE.html#a614cf6d18eb689d78f3f793b1d30eec5", null ],
    [ "lin_acc", "classbno055__base_1_1BNO055__BASE.html#af57984a92baebed0a43ac0d89d44f9bd", null ],
    [ "mag", "classbno055__base_1_1BNO055__BASE.html#a946856083839b9b5283f2a07ece000f9", null ],
    [ "quaternion", "classbno055__base_1_1BNO055__BASE.html#a1a8bd31b74be4c970b768a8a2f97347b", null ]
];