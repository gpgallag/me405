var classmcp9808_1_1MCP9808 =
[
    [ "__init__", "classmcp9808_1_1MCP9808.html#a7130d6eff647c74bb46010b154a5e873", null ],
    [ "celsius", "classmcp9808_1_1MCP9808.html#abec2aa7008fec942521d9fd54e7547b1", null ],
    [ "check", "classmcp9808_1_1MCP9808.html#a7f0be9605522cf82ad16697595154118", null ],
    [ "farenheit", "classmcp9808_1_1MCP9808.html#a2066c32e49d7755c10f5a0f38ea8ad15", null ],
    [ "buffy", "classmcp9808_1_1MCP9808.html#a13b12bbd5531eb2f53738b3db6ec697e", null ],
    [ "I2C", "classmcp9808_1_1MCP9808.html#aabdc64324fd08dfd6bc4323f24d529cc", null ],
    [ "MCP9808_address", "classmcp9808_1_1MCP9808.html#a150ca961af86b7d24c8c87389b083507", null ],
    [ "mfg_ID", "classmcp9808_1_1MCP9808.html#a5e123e5dcb2f8d0663f0e6f5812edc77", null ]
];