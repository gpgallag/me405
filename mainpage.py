## @file mainpage.py
#  Brief doc for mainpage.py
#  
#  Detailed doc for mainpage.py 
#  
#  @mainpage
#
#  @section sec_intro Introduction
#  Welcome! This html website serves as documentation for code developed by
#  Grant Gallagher in ME405: Mechatronics - taught in conjuction by Charlie Refvem and John Ridgley.
#  
#  @section sec_details Portfolio Details
#  The following sections represent my portfolio. See individual modules for details. \n \n
#  \b Modules:
#  - @ref lab0x01
#  - @ref lab0x02
#  - @ref lab0x03
#  - @ref lab0x04
#  - @ref lab0x05
#  - @ref lab0x06
#  - @ref lab0x07
#  - @ref lab0x08
#  - @ref lab0x09
#
#  @section respository Source Code Repository
#  Here is a complete repository containing the source code of each module:
#  https://bitbucket.org/gpgallag/me405/src/master/ \n \n
#  Alternatively, you can find the source code (and documentation) for each module
#  at the bottom of their respective page under @ref sec_details.
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#  
#
#  @page lab0x01 Lab 0x01: Vendotron Finite State Machine
#  @section sec_lab0x01_summary Summary
#  The objective of this assignment is to develop a program that embodies the function
#  of the vending machine from a finite state diagram. Finite state diagrams are high–level
#  design tools that can be used to ensure that code will adhere to necessary design requirements.
#  As a finite state machine should, it is crucial that the Vendotron runs cooperatively;
#  the code does not contain any blocking commands.
#
#  @section sec_lab0x01_design_requirements Design Requirements
#  The vending machine has several buttons, including one for each kind of drink
#  that is available. Additionally there is a small two-line LCD text display and
#  coin return button as you would expect on an average vending machine. Consider
#  the figure below.
#
#  @image html VendotronFigure.png "Figure 1. A visual representation of the Vendotron (Source: Lab 0x01 Handout)"
#
#  In addition to running cooperatively (does not include any 'blocking' commands)
#  the Vendotron must account for the following behavior:
#  - On startup: Vendotron displays an initialization message on the LCD screen.
#  - At any time a coin may be inserted: When a coin is inserted, the balance displayed
#    on the screen must reflect so.
#  - At any time, a drink may be selected:
#    -# If the current balance is sufficient, Vendotron vends the desired beverage through the flap at the bottom of the 
#       machine and then correct change is provided through the coin return.
#    -# If the current balance is insufficient then the machine displays an Insufficient Funds
#       message and then displays the price for the selected item.
#  - At any time the Eject button may be pressed: When the eject button is pressed,
#    the machine returns the full balance through the coin return. The balance should
#  contain change in the least number of denominations. \n
#
#  @section sec_lab0x01_inputs User Inputs
#  The acceptable user input keystrokes are as follows. \n
#  <b> Beverage Selection: </b> \n
#  'C' - select Cuke \n
#  'P' - select Popsi \n
#  'S' - select Spryte \n
#  'D' - select Dr. Pupper \n
#
#  <b> Return Change </b> \n
#  'E' - eject change \n
#
#  <b> Insert Payment </b> \n
#  '0' - insert one penny \n
#  '1' - insert one nickle \n
#  '2' - insert one dime \n
#  '3' - insert one quarter \n
#  '4' - insert one dollar bill \n
#  '5' - insert one five-dollar \n
#  '6' - insert one ten-dollar bill \n
#  '7' - insert one twenty-dollar bill
#  
#  @section sec_lab0x01_FSD Vendotron State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram for the Vendotron.
#  @image html VendotronFSD.png "Figure 2. Vendotron Finite-State Transition Diagram"
#
#  @section lab0x01_documentation Documentation
#  Here is the documentation of this lab package: Lab0x01
#  @section lab0x01_source Source Code 
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me405/src/master/Lab0x01/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x02 Lab 0x02: Think Fast!
#
#  @section sec_lab0x02_summary Summary
#  The objective of this assignment is to write embedded code which responds very
#  quickly to an event using MicroPython. Because the program must respond so quickly,
#  interrupts are used to implement a small part of the code. The goal is to very accurately
#  measure a person’s reaction time responding to a light and pressing a button.
#
#  @section sec_lab0x02_design_requirements Design Requirements
#  The program should wait a random time between 2 and 3 seconds, then turn on
#  the little green LED connected to microcontroller and start timing with a timer
#  that counts microseconds. The program should illuminate the LED for one second.
#  An external interrupt should be set up on pin PC13, which is connected to the
#  blue User button on the Nucleo. When the button is pressed, it causes a falling
#  edge on the pin, so an interrupt should be triggered by the falling edge; the
#  interrupt callback function should read the timer to see how many microseconds
#  have elapsed since the LED was turned on. The process of turning the LED off
#  and on and measuring reaction times should repeat until the user presses Ctrl+C 
#  to stop the program. When the program is stopped, the average reaction time
#  over the number of tries should be calculated and displayed. An error message
#  should be printed if no reaction time was successfully measured (such as if
#  the button wasn’t pressed); the program should not crash just because the user
#  did not use it correctly. Due to the nature of this script, it is allowable to use
#  delay loops. Typically, you would not see such a delay loop in multitasking code,
#  as it ties up the CPU for the duration of the sleeping - but this script does 
#  not feature multitasking.
#
#  @section lab0x02_documentation Documentation
#  Here is the documentation of this lab package: Lab0x02
#  @section lab0x02_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/gpgallag/me405/src/master/Lab0x02/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x03 Lab 0x03: Pushing the Right Buttons
#
#  @section sec_lab0x03_summary Summary
#  The objective of this assignment is to write a script which records a user's
#  button press with high precision to examine effects of button bounce. With the
#  proliferation of digital technology many devices nowadays use (analog) user inputs
#  to trigger events of interest. Though the timing of such events may not always
#  be critical (e.g., sensing touch to create a text message), other events must
#  be controlled with high precision (e.g., deploying an airbag during a traffic collision).
#  In this lab, I will extend the use of interrupts, seen in Lab0x02, to examine the voltage
#  response of the Nucleo™ to a button press, by way of theanalog-to-digital converter (ADC).
#
#  @section sec_lab0x03_design_requirements Design Requirements
#  In this lab, the user will open and run a Python file in Spyder that acts as
#  the UI front end, communicating with the Nucleo via serial communication. There
#  will be a secondary program running on the Nucleo with an objective to collect, store,
#  and transmit voltage respond of button step response. When the user presses
#  the \'G\' key from the console in Spyder, the Nucleo will begin waiting for a the
#  user to press the blue button. Once the user button is pressed on the Nucleo, it
#  will capture the input voltage (ADC counts) and store them in an array. Once the
#  array has been created and the data has been collected, the batch of data will then
#  be transmitted to the laptop through UART. Once recieved on the PC, the data will
#  then be stored as a comma-separated variable (.CSV) file that includes the timestamps
#  associated with each ADC count. In addition to the .CSV, the data will be plotted and
#  labeled appropriately using the matplotlib module.
#
#  @section sec_lab0x03_results Results
#  After initiating the design procedure outlined above, the script outputs a
#  plot and .CSV file of the user-button step response. The step response plot of
#  a demonstration run is shown in Figure 1. below.
#  @image html Lab0x03Results.png "Figure 1. ADC Step Response of user-button press"
#  While the user button is pressed, an ISR is called to record the button voltage
#  and respective timestamps at a frequency of 40 kHz. The button voltage is measured
#  using the Analog-to-Digital converter on pin PA0 of the Nucleo, and the
#  user-button voltage is measured from the output pin PC13. As seen in Figure 1 above,
#  the voltage response is not binary - it does not go from off to on instantaneously.
#  Instead, the response is logarithmic, and has a rise time of around 1 ms. This
#  response can be attributed to the debouncing circuit that is designed into the Nucleo.
#  In order to filter out bounce and other forms of electrical noise, their is a an RCL
#  circuit that smooths out the response to make it more predictable.
#
#  @section sec_lab0x03_UI_FRONT_FSD UI_Front State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram for front-end user
#  interface with runs locally on Spyder.
#  @image html UIFrontFSD.png "Figure 2. Front-End User Interface Finite-State Transition Diagram"
#
#  @section sec_lab0x03_UI_BACK_FSD UI_Back State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram for back-end user
#  interface/data collection task that runs remotely on the Nucleo.
#  @image html UIBackFSD.png "Figure 3. Back-End User Interface (Data Collection) Finite-State Transition Diagram"
#
#  @section lab0x03_documentation Documentation
#  Here is the documentation of this lab package: Lab0x03
#  @section lab0x03_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/gpgallag/me405/src/master/Lab0x03/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x04 Lab 0x04: Hot or Not?
#
#  @section sec_lab0x04_summary Summary
#  The objective of this assignment is to write a script for the Nucleo which
#  measures the internal temperature of the micro-controller through onboard modules,
#  measures the ambient temperature of the room using an MCP9808 temperature sensor
#  through I2C communications. For this assignment, I have developed two scripts:
#  a driver temperature sensor called mcp9808.py and a main script which
#  logs temperature data from the Nucleo and MCP9808 over a large
#  span of time in 1-minute intervals. Finally, the temperature data is exported
#  as a .CSV file and plotted.
#
#  @section sec_lab0x04_I2C I2C Communications
#  Similar to the MCP9808, many other sensors
#  (accelerometers, IMU’s, etc.) use the same communication method as this sensor.
#  An MCP9808 on a breakout board can be used with only its power, ground, an two
#  I2C communication pins as shown in Figure 1. below.
#  @image html I2C_Diagram.png "Figure 1. Example of an I2C circuit integrated with the Nucleo MCU (Source: Lab 0x04 Handout)"
#  The I2C interface uses two wires to carry data to and from sensors or other
#  devices that work with a microcontroller. It is used over short distances,
#  typically less than a meter or so. I2C communications use two wires: SDA and SCL.
#  SDA stands for the Serial Data line, which carries data one bit at a time in any needed direction.
#  SCL stands for the Serial Clock line, which synchronizes data transmission/reception.
#  The I2C interface is a \a bus, meaning that the SDA and SCL lines may be shared
#  between  multiple devices. To distinguish between different devices, each device
#  is assigned a 7-bit I2C bus address. The address may be set at the factory;
#  many devices have address control pins which can be used to change the address,
#  allowing several devices of one type to share an I2C bus. A few devices can have
#  their addresses programmed by the user. An I2C communication begins with a start
#  condition, and the microcontroller sending the 7-bit address of the device with
#  which it will exchange data. Along with the address, an eighth bit indicates
#  whether the microcontrollerwill write data to the device or read data from it;
#  then, one or more bytes are exchanged.
#
#  @section sec_lab0x04_design_requirements Design Requirements
#  - Write a script for your microcontroller which measures the \a internal temperature
#    of the micro-controller using the pyb.ADCAll class and saves it to a .CSV file. 
#  - Write  a Python module containing a driver class which allows the user to communicate
#    with an MCP9808 temperature sensor using its I2C interface. The class should contain
#    an initializer, a method to verify that the sensor is connected, a method to measure
#    the temperature in degrees Celsius, and a method to meaure the temperature in
#    degrees Farenheit
#  - Write a main script which uses the MCP9808 driver class and script to measure the internal MCU
#    temperature to take temperature readings approximately every sixty seconds from the
#    STM32 and from the MCP9808 and saves those readings as a .CSV file. The CSV 
#    file’s columns should be time, STM32 temperature, and ambient (MCP9808) temperature.
#    Data should be taken until the user presses Ctrl+C, at which time the program
#    should cleanly exitwith no errors.
#  - Take data for a period of at least eight hours, then retrieve the data file
#    from your microcontroller and use any desired means to plot it.
#
#  @section sec_lab0x04_results Results
#  Due to shipping difficulties, the data collection from the MCP9808 was delayed.
#  I was able to order a second breakout board from a different supplier, which managed
#  to arrive much sooner than the first order. Nonetheless, the recording was successful.
#  Two temperatures were measured over a duration of 8 hours. First, the core temperature
#  of the Nucleo was measured on-board using the pyb class. Second, the ambient temperature of
#  the room was measured using the MCP9808 chip. The results are shown below.
#  @image html Temperature_Plot.png "Figure 2. Temperature readings of a Nucleo L476RG core processor and MCP9808 temperature sensor taken between 11:00pm and 7:00am on 2/17/21 in San Luis Obispo."
#  The most notable discrepency between the data sets is that the core temperature of
#  the Nucleo is approximately 5 °C higher at an given point. This can be attributed to the
#  fact that the Nucleo's core gives off a small amount of heat as it computes; however,
#  this difference can also be because of the accuracy of the temperature sensor. The MCP9808
#  is a much more precise device. This brings up the second point - it's very clear to see
#  the resolution of the core temperature readings. The Nucleo's ADC is only 12 bits,
#  which means that the temperature has a resolution of 0.0625 °C. Meanwhile, the MCP9808
#  has a much finer resolution. After all, the MCP9808 (which is solely a temperature sensor)
#  is about 2/3rds the price of the Nucleo after shipping and taxes. Lastly, there
#  is a strange hump at around 4:45am. I am not entirely sure what would cause this
#  outlying data set, but I could hypothesize that the change in temperature is likely due
#  to the heat being pumped out of my PC during an overnight update (the Nucleo was
#  on my desk, next to my PC). 
#
#  @section lab0x04_documentation Documentation
#  Here is the documentation of this lab package: Lab0x04
#  @section lab0x04_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/gpgallag/me405/src/master/Lab0x04/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x05 Lab 0x05: Feeling Tipsy?
#
#  @htmlinclude Lab0x05_Documentation.html
#
#  @section lab0x05_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/gpgallag/me405/src/master/Lab0x05/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x06 Lab 0x06: Simulation or Reality?
#
#  @htmlonly
#  <embed src="Lab0x06_Documentation.html" width="100%" height="11500px" href="Lab0x06_Documentation.html"></embed>
#  @endhtmlonly
#
#  @section lab0x06_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/gpgallag/me405/src/master/Lab0x06/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x07 Lab 0x07: Feeling Touchy
#
#  @section sec_lab0x07_summary Summary
#  The objective of this lab is to assemble the hardware kit for the ball balancing
#  system, and to write a hardware driver to interface with the resistive touch panel
#  used to measure the planar position of the ball. While most touchscreen devices
#  today use capacitive touch panels for their multi-touch input, resistive touch panels
#  still have their benefits. Resistive panels are cheaper, work well with gloves and 
#  when wet, and are simple to interface with a modern microprocessor.
#
#  @section lab0x07_hardware_assembly Hardware Assembly
#  While a majority of the system was shipped preassembled, there were a piece of
#  hardware that needed to be assembled before moving forward. The kit arrived with
#  the base and platform linkage already joined together. Similarly, the kit was
#  fabricated with two BLDC motor/encoder assemblies and a custom PLC breakout board
#  with a STM32 L476RG microprocessor (from ME305). Left to be assembled was the motor
#  arm linkages, BNO055 IMU breakout board, resistive touch panel, and FPC touch breakout board.\n
#
#  First, I attached the fiberglass motor arms by fastening the clamp onto the
#  encoder shaft. Subsequently I connected the motor arms and platform by fastening
#  the double-ended ball joint rods to each component. The assembly of the motor
#  arm linkage is shown in Figure 1 below.
#  @image html MotorArmLinkage.jpg "Figure 1. Assembly of the motor arm linkage for one axis of the platform." width=400cm
#  Now, each motor has direct control over one degree of freedom of the platform. Next,
#  I soldered the extended double-ended pin headers to both the FPC and BNO055
#  breakout boards. I soldered the pin headers such that the
#  extended header end is facing upwards – allowing for extended cable connectivity.
#  With the breakout boards pin headers attached, I soldered the bottom pins to the
#  top of the platform so that they are securely mounted. It is crucial that the
#  IMU breakout board is mounted securely, and level, to the platform so that it
#  can accurately measure the angular position of the platform for calibration.
#  To attach the resistive touch panel, I removed the protective film from the backside
#  (leaving the thick, screen protector on) and mounted it to the top of the
#  platform with three 3M adhesive strips. Finally, connected the touch panel cable
#  to the FPC breakout board, and connected the two breakout boards to the headers
#  on the Nucleo breakout board via 4-pin extension cables. The fully-assembled ball
#  balancing hardware kit is shown in Figure 2 below.
#  @image html AssembledHardware.jpg "Figure 2. Fully assembled hardware kit for 2-DoF ball balancing system (ball not shown)." width=400cm
#
#  @section lab0x07_resistive_touch_panel Resistive Touch Panel
#  With the hardware kit fully assembled, the next portion of this lab is to
#  write a script that scans the touch panel for the position of contact (with the ball).
#  RTPs can be thought of like a voltage divider – or potentiometer. Take
#  Figure 3 as a simplified schematic of an RTP,
#  @image html SchematicRTP.png "Figure 3. Schematic representation of a 4-wire resistive touch panel (Source: Lab 0x07 Handout)."
#  Where the junction in the center is the point of contact with the panel. To scan
#  for the x position, the circuit between \f$x_p\f$ and \f$x_m\f$ must be energized.
#  \f$x_p\f$ is set to 3.3V, and \f$x_m\f$ is set to 0V. Then, the voltage at the junction
#  can be measured by either pin \f$y_p\f$ or \f$y_m\f$. In either case, the fourth pin
#  must be floated. As the point of contact moves left, the resistance between
#  \f$x_p\f$ and the junction becomes smaller, and the measured voltage becomes greater.
#  Assuming that the resistance across the entire touch screen is uniform, we can
#  then assume that the voltage scales linearly with the position – creating a
#  direct translation between position and measured voltage. Subsequently, both
#  the x and y components of the touch screen can be measured by rapidly alternative
#  between energizing the circuit between opposing pins, measuring the voltage
#  of an adjacent pin, and floating the last pin.
#
#  @section lab0x07_calibration Calibration
#  As with many engineering applications, there can be discrepancies between reality and theory.
#  For the most part, the assumption that the resistance of the touch panel is uniform is
#  valid therefore making position scale linearly with measured voltage. However, due to
#  the fabrication of the touch panel, there are \"dead zones\" near the edges in which
#  the Nucleo is not able to read any positive voltage. Indeed, the manufacturer
#  specifies the active region dimensions in their datasheet, pictured below.
#  @image html TouchPanelDatasheet.png "Figure 4. ER-TFT080-1 Datasheet from EastRising Technology Co. LTD." width=500cm
#  Nonetheless, due to defects from manufacturing, shipping, and other factors,
#  the active area of my touch panel is different. To account for these differences,
#  I developed my own method of calibration. With two points of contact with the touch panel,
#  I am able to measure their distances and respective ADC counts. Then, I can
#  develop a ratio that between distance and ADC,
#  \f[
#  \frac{x_2-x_1}{c_2-c_1}
#  \f]
#  where \f$x\f$ is position at the contact point and \f$c\f$ is the measured count
#  at the contact point. Next, I can offset the zero-point by taking the difference
#  between the count at the center of the platform and the measured count,
#  \f[
#  c_{center}-c_{meas}
#  \f]
#  where \f$c_{center}\f$ is the measured count at the center of the platform, and
#  \f$c_{meas}\f$ is the measured count at the point of contact. Because the touch panel
#  was designed to be mounted in the way that it is assembled, the raw
#  measured count increases in the negative x and y direction. Subsequently, I
#  subtracted the X from the Y so that the direction adheres to the axes I assumed
#  in my previous calculations (SEE LAB 0x06). Finally, I can combine these two
#  relationships to create an equation that maps ADC count to physical position,
#  \f[x_{position}=(c_{center}-c_{meas})\frac{x_2-x_1}{c_2-c_1}\f]
#  where \f$x_{position}\f$ is the distance from the center of the platform as a
#  function of the measured ADC count, \f$c_{meas}\f$. Subsequently, this equation
#  can be used to calculate the position in both the x and y axes.
#  To accurately obtain the data needed to calibrate the position along the touchscreen,
#  I created my own adhesive guide. The guide was carefully drawn in AutoCAD so
#  that all of the dimensions, shown in Figure 5 below, are deliberate.
#  @image html CalibrationGuide.png "Figure 5. AutoCAD drawing of adhesive touch panel calibration guide."  width=400cm
#  The drawing was then exported to a Cricut cutting plotter, where the design was
#  cut from of adhesive paper. As a result, adhesive guide had a high tolerance (I confirmed the physical
#  dimensions with dial calipers). Next, I could place the guide on the center
#  of the touch panel. However, I had to be careful about placement for a variety
#  of reasons. Because the panel was placed on the platform by hand, the geometric
#  center of the touchscreen may very well be offset from the geometric center of
#  the platform (imagine placing the panel along the corner edge of the platform).
#  Moreover, the geometric center of the touch panel is not the center of the
#  resistive circuit. Along the edge of the touch panel where the FPC cable is,
#  the circuit is offset by a few millimeters, cause the center of the circuit
#  to be offset as well. For these reasons, it was crucial to measure raw ADC
#  count at the point of contact in the center of the platform after it has been
#  adhered. Due to these complexities, I placed the guide in the center of the
#  platform visually, while taking extra precaution to align the axes with the
#  touch panel using a try square. The adhesive guide can be seen on the touch panel below.
#  @image html GuideOnPlate.jpg "Figure 6. Adhesive touch panel calibration guide centered on platform." width=400cm
#  With the adhesive template on the touch panel, I was finally able to obtain
#  relatively accurate calibration data for my touch panel. To obtain the data,
#  I ran the touch panel driver script with minor modifications. The modified
#  script actively recorded the ADC count in the x and y axes in a CSV file if
#  contact was detected. For the calibration, I used a fine-point stylus to
#  follow the guide grooves along the template while recording the raw measured
#  counts – making sure to spend extra time in-contact with the end points of the
#  groove and center point of the guide. The results are shown below.
#  @image html CalibrationPlot.png "Figure 7. Raw calibration plot of the resistive touch panel measuring ADC count of contact points spanning 20mm in each direction."
#  In the horizontal direction, at distances 20mm away from the center point in both directions,
#  the ADC counts were measured to be 1636 and 2452 respectively. In the vertical direction,
#  at distances 20mm away from the center point in both directions, the ADC counts
#  were measured to be 1291 and 2585 respectively. Finally, the ADC count with
#  respect to the center of the platform was measured to be approximately 2050 in the
#  horizontal direction, and 1939 in the vertical direction. \n
#
#  It’s clear to see that the center of the touch screen does not occur at half of
#  the maximum ADC count (\f$4095/2\f$) as one would expect. Moreover, these results
#  demonstrate that the scaling factor in the vertical direction is not the same
#  as that in the horizontal direction. With these results, I calculated the
#  conversion equation for the horizontal (x) position to be,
#  \f[x_{position}=(2046-c_{meas})\frac{20-(-20)}{2452-1636}\f]
#  and the conversion equation for the vertical (y) position to be,  
#  \f[y_{position}=(1935-c_{meas})\frac{20-(-20)}{2585-1291}\f]
#
#  @section lab0x07_results Results
#  After calibrating the script to perform accurately with my touch panel,
#  I was able to obtain very accurate results. With the refined touch panel driver,
#  I followed the same recording procedure outline in the Calibration. Using the
#  adhesive guide template that was precisely cut with a Cricut, I was able to carefully
#  trace a 40mm x 40mm cross about the center of the platform. The results are shown below.
#  @image html CalibratedPlot.png "Figure 8. Calibrated plot of the resistive touch panel measuring position of contact points spanning 20mm in each direction."
#  The results from the calibration were as accurate as expected. Following the template,
#  the measured position along the touch panel replicate a cross, protruding
#  from the center point by 20mm along each axis. While the touch panel may not
#  be as accurate along the outer edges, it is okay since I am assuming that
#  the ball will stay relatively close to the equilibrium point (about the center).
#  Additionally, while it would be more accurate to calibrate the touch panel
#  using points that are spaced farther apart, I noticed that the measured voltage
#  began to behave non-linearly closer to the outside regions of the plate.
#  Once again, since the system will operate closely about the equilibrium point,
#  the level of accuracy for these results is acceptable. \n \n
# 
#  In order to work cooperatively with other code used to control the ball balancing system,
#  the RTP driver must be able to scan each channel very quickly. To measure the latency of driver,
#  I created a brief script that measures the average time it takes to scan the RTP
#  (outside of the method) over a span of 1000 trials. Initially, I used a method which calls
#  the functions that scan for the X, Y, and Z components individually - I measured the average
#  latency to be about 863+/-5 μs. After cleaning up the code for a bit of time (by importing
#  libraries and configuring pins more efficiently), I was able to reduce the latency to
#  730+/1 μs. While this is faster, my goal was to reduce the latency to under 500 μs for all three channels.
#  Subsequently, I created a secondary scanning method that does not call any other class methods,
#  and scans the channels in the most efficient sequence possible. With this method, I was able
#  to reduce the latency of the RTP scanner to 487+/-1 μs over a span of 1000 trials. I tested the
#  accuracy of this new scanning method, which has no delays or filtering, by tracing the calibration
#  template as discussed previously (see @ref lab0x07_calibration) which had succesful results.
#  With the driver latency under 500 μs and the positional accuracy within +/-0.5mm, I am
#  extremely satisfied with the results.
#
#  @section lab0x07_documentation Documentation
#  Here is the documentation of this lab package: Lab0x07
#  @section lab0x07_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/gpgallag/me405/src/master/Lab0x07/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x08 Lab 0x08: Term Project Part I
# 
#  @section lab0x08_summary Summary
#  The objective of this lab is to create and develop drivers for the BLDC motors
#  and optical encoders that are integrated with the project hardware kit. These
#  two drivers are critical to the foundation of the control system since they are
#  used to directly interact with the hardware. The two BLDC motors are the only
#  actuators among the entire system; thus, it is critical that the driver runs quickly,
#  predictably, and cooperatively. Subsequently, the resistive touch panel and optical encoders
#  (and IMU) are the only inputs to the system and must and must also behave with similar
#  accuracy to the motors. The optical encoders provide the most accurate/precise positional
#  measurements of the system, which are used to monitor the angular position and velocity of
#  the platform. Finally, the motor, encoder, and touch panel drivers are used to implement
#  a control system that is based on the model developed in @ref lab0x06. \n\n
#
#  Luckily, the majority of the motor and encoder drivers has already been
#  developed and tested in ME305 (Fall 2020). To review those labs, you can check out
#  <a href="gpgallag.bitbucket.io/ME305/lab0x03.html"> <b> Lab 0x03: Encoder Driver</b></a> and
#  <a href="gpgallag.bitbucket.io/ME305/lab0x06.html"> <b>Lab 0x06: DC Motors </b></a>. 
#
#  @section lab0x08_hardware_connections Hardware Connections
#  For quick reference, the tables outlining the relationships between the hardware is shown below.
#  The relationship of pinout connections for the hardware kit is shown in Table I. below (Source: Lab 0x08 Handout).
#  @image html pinout_tables.png Table I. Connections between the BLDC motors, DRV8847 motor driver chip, and Nucleo (left) and the connections between the optical encoders and Nucleo (right). width=600cm
#  While, the truth table (and much more information) for the <a href="https://www.ti.com/lit/ds/symlink/drv8847.pdf?ts=1615433762697&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FDRV8847"> <b>DRV8847 Motor Driver Chip </b></a>
#  is in the datasheet, located on the manufacturer's website, a quick reference
#  to the table is shown in Table II. below.
#  @image html truth_table.png Table II. 4-Pin configuration truth table for the DRV8847 motor driver chip to drive two BDC motors with PWM. width=700cm
#
#  @section lab0x08_fault_detection Fault Detection
#  While the heart of the drivers remained unchanged, there were minor improvements
#  made to the constructors to reduce clutter and improve efficiency. However,
#  there was one \b major addition to the motor driver class in the implementation
#  of fault detection. Fault detection is a method of system diagnostics that 
#  detects potential hardware failure and mitigates damage. In this case, the
#  <a href="https://www.ti.com/lit/ds/symlink/drv8847.pdf?ts=1615433762697&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FDRV8847"> <b>DRV8847 Motor Driver Chip </b></a>
#  comes with a number of built-in protection features, such as undervoltage lockout,
#  open load detection, and thermal shutdown. This lab will focus on utilizing the 
#  fault condition indication pin (nFAULT) to detect a fault in the system, most 
#  often caused by an over-current condition. \n\n
#
#  The nFAULT pin is asserted active-low with a pull-up resistor. This means that
#  nFAULT is read as low (0) when there is a fault detected, and read as high (1)
#  under normal conditions. In order to properly utilize the fault detection, an 
#  Interrupt Service Routine (ISR) was integrated into the motor driver class – as 
#  seen in @ref lab0x02 and @ref lab0x03. Since nFAULT is active-low, this means that 
#  a fault occurs when the nFAULT pin transitions from high (1) to low (0).
#  As a result, the interrupt callback is designed to be triggered when a *falling edge*
#  is detected on nFAULT. Then, to protect the functionality of the hardware, 
#  all motor functionality is disabled while the fault is active. In order to resume, 
#  the user must clear the fault by deliberately pressing the ENTER key. 
#  Additionally, if the fault remains active when the user presses ENTER 
#  (which means that the issue was not fixed), the motor functionality remains 
#  disabled until the issue that is causing the fault is cleared (in addition to pressing ENTER).
#
#  @section lab0x08_results Results
#  In addition to the extra hardware components that were implemented since ME305 (Fall 2020),
#  the motors and encoders behave much more predictably. The drivetrain has much less 
#  friction/tension than previously. As a result, the motors are capable of going 
#  from 0-100% duty cycle without stalling – they can also operate at slightly 
#  lower duty levels of ~25% as compared to ~35% last year. Additionally, the encoders 
#  no longer malfunction or cut-out after continuous use. Subsequently, the motors 
#  and encoders (and their drivers) perform as expected. They are accurate, reliable, 
#  and cooperative. Included in the motor/encoder drivers are code blocks which 
#  can be used to test and debugs the components. \n\n
#
#  The fault detection system also responds in an appropriate fashion. For testing, 
#  the blue user button (PC13) was used in replacement of the nFAULT pin to simulate
#  the triggering of a fault condition. This way, the fault condition can be triggered 
#  in a repeatable fashion that does *not* cause possible damage to the hardware – the 
#  user would have to do something potentially dangerous, such as grabbing the output 
#  shaft at 100% duty cycle, to generate an over-current. If the fault detection were 
#  to not be set up properly, this could destroy the <a href="https://www.ti.com/lit/ds/symlink/drv8847.pdf?ts=1615433762697&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FDRV8847"> <b>DRV8847 Motor Driver Chip </b></a>
#  and/or damage the motor windings. When the user button is pressed while the test is running, 
#  the user is prompted with a fault message and all motor functions are disabled 
#  until the fault is fixed *and* the user pressed the ENTER key in the REPL. 
#  Thus, the fault detection system works appropriately for both momentary and long-term fault conditions.
#
#  @section lab0x08_documentation Documentation
#  Here is the documentation of this lab package: Lab0x08
#  @section lab0x08_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/mitchellkcarroll/term-project/src/master/
#  - - -
#  @author Grant Gallagher
#  @author Mitchell Carroll
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x09 Lab 0x09: Term Project Part II
# 
#  @section lab0x09_summary Summary
#  This project is the culmination of all previous labs, and the second part of
#  @ref lab0x08. The objective of this lab is to integrate the hardware, drivers,
#  and model of the 2Dof ball balancing system developed throughout this quarter
#  in order to implement a real full-state feedback control system. The positional
#  states of the system can be measured by the optical encoders, resistive touch panel, 
#  and IMU. Subsequently, the velocities of the system can be computed from the difference 
#  in positional measurements and the time intervals between them. Finally, a 
#  full-state feedback controller can be implemented to determine the PWM level to send 
#  to the motors – which will control the system. Ideally, the system will be able 
#  to balance a ball on the platform and respond appropriately to minor disturbances.\n\n
#
#  @section lab0x09_controller_tuning Controller Tuning
#  While it is ideal to determine the gains of the controller analytically,
#  there are many discrepancies between our model and our real system. As a result, 
#  the gains that we determine through analytical measures will not be very accurate 
#  and should serve as a starting point. To calculate a starting point for the controller 
#  gains, we followed a procedural guide written by our instructor Charlie Refvem titled, 
#  "Gain Calculation by Char. Polynomial Matching." The following steps are taken in reference to the guide. \n\n
#  
#  First, we recall our system matrix A, and control matrix B developed in @ref lab0x06.
#  Next we chose two arbitrary values of the damping coefficient, zeta, and the natural
#  frequency, wn. We chose the natural frequency and the damping coefficient to be 5 and 0.8
#  respectively (Note: these values are not accurate, as the system is extremely simplified). 
#  With these values, we determined the pole locations for the desired second order system. 
#  Then, we choose to set the remaining two pole location to be 10x larger than the first two 
#  real components. Finally, we inputted these four pole locations, along with the state and 
#  control matrices of the system, into the built-in MATLAP function, place(). The returned 
#  value contains a 1x4 array of controller gain values that we used as a baseline for tuning 
#  the controller. The steps can be seen below.
#  @image html gainTuning.png 
#  Nonetheless, these values were drastically different than the actual gains used for
#  our real controller. To begin with, we had to includer a motor gain. The motor gain is
#  responsible for converting the units of torque to a percentage of duty cycle. By using
#  the specifications on the
#  <a href="https://www.maxongroup.com/medias/sys_master/root/8841086861342/EN-89.pdf"> <b> motor datasheet </b></a>,
#  the motor gain can be calculated by dividing the resistance of the motor windings by 
#  the motor voltage and torque constant. As a result, we calculated a motor gain value of 
#  approximately 13.345 amps per newton-meter. Thus, the controller must be multiplied 
#  by the total motor gain.\n\n
#
#  Subsequently, we tuned the state gains through numerous trials. Starting with
#  the gains associated with the platform, and then incorporating the gains associated
#  with the ball. Throughout this process, we were careful to document the effects of each 
#  gain value – slowly incrementing each one at a time. Finally, we concluded on a controller 
#  gain matrix of [-40, -0.8, -100, -12] for the velocity of the ball, velocity of the platform,
#  position of the ball, and position of the platform, respectively. It is very important
#  to note that we made slight alterations to the hardware on our system to improve performance:
#  such as greasing the ball joints and rebalancing the weights on the platform.\n\n
#
#  Additionally, we implemented partial integral control of the ball’s position into
#  our full-state feedback controller. We noticed that the ball would not converge towards
#  the center of the platform for some runs. Additionally, the ball converged to different
#  locations after every calibration sequence. We figured that the difference in equilibrium
#  position had to do with the inclination of the platform, as well as the level of the
#  platform when the encoders are zeroed. As a result, the system is capable of responding
#  to prolonged disturbances (as long as the are small), and the ball always converges towards
#  the center of the platform. We used an integral gain value of 0.5 for the ball’s position
#  (not including the motor gain).
#
#  @section lab0x09_challenges Challenges
#  <b>Coordinate System</b> - The first challenge that arose while programming the controller was that the axis
#  of some of the hardware was "flipped". In order to keep a firm grasp on the kinematics
#  of the system, all of the calculations for the system were done with a consistent coordinate
#  system. The origin of the coordinate system is located at the U-joint of the platform,
#  the x-axis and y-axis pass through each of the motors/encoders, and the z-axis passes out
#  of the touch panel. An illustration of the coordinate system is shown in Figure 1 below.
#  @image html systemCAD.png Figure 1. CAD render of pivoting platform. The first motor spins about its x-axis but tilts the platform about its y-axis and the second motor spins about its y-axis but tilts the platform about its x-axis. (Source: Charlie Refvem) width=500cm
#  The resistive touch panel was designed with the opposite sign convention.
#  While there are many ways to account for this issue, such as rewiring the header
#  pins connected to touch panel, we fixed this issue by reinitializing the pin objects 
#  associated with the RTP driver class. This allowed us to measure the ball's displacement 
#  and velocity, with respect to the touch panel, in the same (rotated) coordinate system 
#  as the rest. Luckily, a positive duty cycle for both motors and encoders correlate to a
#  positive rotation about their respective axes; however, the reaction of the platform 
#  does not. For the motor located along the x-axis, a positive rotation correlates to a 
#  negative rotation of the platform along the y-axis. While the solution is easy as reversing 
#  the sign convention for the duty cycle applied to that motor, forgetting to do so would yield 
#  catastrophic results. Essentially, the motor would apply the corrective torque in the opposite 
#  direction as intended, which could lead to a faulted state and possibly damage to the hardware.
#  Finally, the BNO055 IMU also had discrepancies between relative coordinate systems.
#  We chose to solder the IMU to the platform as (designed) to ensure that it was sturdy
#  and parallel to the platform. As a result, the relative coordinate system of the
#  IMU was drastically different than the system. While we could have mounted the board
#  in a different orientation using tape or adhesive, this would have been less secure,
#  and could have possibly introduced noise and error to the readings while leveling the
#  platform and calibrating the encoders. To account for this issue, we made sure to
#  accurately keep track of which Euler angle readings correlated to the behavior of the system.\n\n
#  
#  <b>Latency</b> - When implementing a control system, the latency and scheduling of system tasks is critical.
#  Before setting the duty cycle of the motors, the states of the system must be accurate and recent,
#  and there cannot be a significant lag in timing. While running controller for the system, the 
#  smallest interval timing for the controller (while controlling the platform) that we were able 
#  to achieve was approximately 9ms. This means that it takes ~9ms to read from the touch panel, read 
#  from the encoders, compute the velocity of the ball and platform, compute the levels of the motors, 
#  and finally actuate the motors. While it would be more ideal to increase the sample
#  rate of the controller (decrease interval), there are some practical challenges. The
#  first challenge that comes with a faster sample rate is the resolution of the readings.
#  First, the touch panel and optical encoders both read *discrete* positional measurements - the
#  optical encoders have 4000 counts per revolution (CPR), and the touch panel measures an integer
#  value between 0 and 4095. Second, the velocities of the ball and platform are computed by the
#  change in position over time. As a result of decreasing the interval between measurements, the
#  resolution of the measured velocity decreases which can lead to poor system control. Alternatively,
#  decreasing the sample rate (increase interval) can also bring about poor system control.
#  Decreasing the sample rate increases the resolution of the velocity measurements, but at the
#  cost decreasing the reaction time of the system. With a greater time in between measurements,
#  the system experiences more noise and perturbations. As a result, the controller may not be able
#  to respond effectively. Taking these two extremes into consideration, we determined that a sample
#  rate of 12ms was a satisfying balance.\n\n
#
#  <b>Timing</b> - In conjunction with latency, the timing of controller tasks is important as well.
#  In order to maximize the response of the controller, we carefully structured the timing
#  and scheduling of each task. While the task controller is set to an interval of 12ms,
#  the actual time between iterations may not be exactly 12ms. To account for this small deviancy,
#  we calculated the actual time between iterations, in microseconds, and used this time to calculate
#  the velocities of the ball and platform. Additionally, the *order* in which each task is executed
#  is important. Before all else, the touch panel is scanned to determine if the ball is
#  in contact with the platform. If the ball is not in contact with the platform, then the 
#  positional readings are wrong, and the controller must account for this immediately. 
#  Subsequently, the encoder positions are also measured at the beginning of each iteration. 
#  This way, all positional measurements are taken at roughly the same time, over the same interval. 
#  Following the readings, the velocities of the system and duty levels of the motors are computed – causing 
#  a small latency. Finally, the duty levels are sent to the motor pins with PWM and 
#  amplified by the DRV8847 motor driver chip. \n\n
# 
#  <b>Steady State Error</b> - After tuning the system for a while, we noticed that there was a small, but noticeable, 
#  steady state error in settling position of the ball. The ball would always settle in a 
#  different location, regardless of whether the platform was re-calibrated. This error 
#  can be attributed to a variety of factors in the system: the platform may not be perfectly 
#  level when calibrated, the weight of the platform may not be balanced, the rubber ball being 
#  used may have flat spots which causes it to settle on non-level surfaces, and/or the motors 
#  may not be able to overcome the static friction in the system in order to make fine adjustments. 
#  As a solution to this problem, we implemented partial integral control into the state feedback 
#  controller, in addition to the proportional state-feedback controller. We applied a small integral 
#  gain to the position and velocity of the ball only, which dramatically improved the steady state 
#  error without hindering the performance. With this addition the ball always stabilizes about the 
#  center of the touch panel when perturbed.  Subsequently, the system behaves normally when an object 
#  is placed on the platform or when the platform is moved to an inclined angle.  \n\n
#
#  <b>Friction</b> - While handling the hardware in-person, we noticed clear discrepancies between our ideal
#  model and reality. The first issue we encountered was the resistance in the system. While 
#  turning the shafts extending from the encoders, there are "hard spots" where it is difficult 
#  to rotate the shaft. We believe that this resistance is cause by tension in the pulley that 
#  connects the motor to the encoder shaft. Subsequently, the sprockets that are connected could 
#  have some form of eccentricity or runout. We also noticed a great deal of friction in the ball 
#  joints of the connecting rods, which connect the motor arms to the platform. When we first 
#  assembled the platform, there was so much friction in the connecting rods that the platform
#  could hold its position without any motor control. It was *very* stiff. To help reduce the 
#  amount of friction, we sprayed the ball joints with WD-40 and continuously worked in the joints. 
#  While there is still a small amount of friction left, it is <b> dramatically</b> less than 
#  what it was initially. \n\n
#
#  <b> Weight Distribution</b> - The mass of the motor arms and connected rods had a more significant effect
#  on the system than we initially thought. When we were first tuning the system, 
#  the platform always fell towards the sides where the motor arms were attached – due 
#  to the weight of the linkages. Subsequently, we also noticed that the steady state 
#  error of the ball's position always trended towards the sides of the motor arms as 
#  well. We concluded that the weight of the linkages could not be neglected in order 
#  to preserve the performance of the controller. To account for this issue, we 
#  strategically moved the aluminum channels, located on the opposing side of the 
#  platform, to counteract the weight of the linkages. For our hardware kits, we 
#  found that shifting the channels over by 4 slots produces a well-balanced platform. 
#  This adjustment helped shift the equilibrium point of the ball back to the center 
#  and reduced the amount of effort needed by the motors to keep the ball at the center 
#  of the platform.\n\n
#
#  <b>Ball Characteristics </b> - One of the most significant sources of instability in our system was the characteristics of the ball.
#  The weight of the ball was often not significant enough to trigger a reading on the resistive touch panel.
#  This effect was amplified when the table made sudden movements to correct the ball’s position.
#  Since the ball is made of rubber, the bounciness played a large role in the instability of the
#  system as well. As the table made sudden movements, the ball was prone to bouncing and causing
#  the table to react unpredictably. The final issue that we had to overcome related to the ball
#  was the flat spots that caused the ball to get stuck in a certain position. Because the ball
#  became stuck, it would never return to the center of the platform exactly, which essentially
#  turns the system in to an inverted pendulum instead of a ball on a table.
#
#  @section lab0x09_results Results
#  After integrating the drivers and knowledge from previous labs, we were able
#  to achieve satisfactory results. Upon powering on, the system immediately begins
#  to calibrate itself – leveling the platform with the IMU and PID control and zeroing
#  the encoders. Upon calibration, the platform holds its position strongly when there is
#  no contact detected. Once there is contact detected between the ball and the touch panel,
#  the system immediately starts to stabilize itself.The system is very robust while controlling the ball. Regardless of whether the platform
#  or ball is hit, the controller is capable of responding quickly. Alternatively, the ball
#  can be placed very close to the outer limits of the touch panel (nearly 8cm from the center)
#  and the system is still able to stabilize.\n\n
#
#  With the inclusion of partial integral control (with respect to the ball’s position)
#  the platform can also manage a wider array of disturbances. If the platform is not perfectly
#  level when the encoders are zeroed, then the integration term can correct for this. Alternatively,
#  small objects such as pencils or LED flashlights can also be placed onto the platform with the
#  ball achieving steady state about the center of the touch panel. Similarly, the platform can be
#  tilted slowly (yes, even after calibration) while controlling the ball, and the ball will
#  stabilize about the center. Of course, these constant disturbances must be applied in
#  small magnitudes over time since the partial integral term is very small.\n\n
#
#  While we were hoping to compare the real system behavior to that of the model in
#  @ref lab0x06, the latency in writing the data to a CSV file severely inhibited our
#  system. While writing to a csv, the interval of the controller was a minimum of 20 ms.
#  With this amount of latency, the system was very shaky and did not behave as it should.\n\n
#
#  Instead of simply talking about the results, here is a link to a video demonstrating the system:  https://youtu.be/pnaUqpFKSWc
#
#  @section lab0x09_discussion Discussion
#  <b>Tuning</b> - While we are satisfied with the response of the system, we
#  believe that we could achieve better results with more system tuning. Primarily,
#  the controller gains could be adjusted to provide more damping to the system which
#  could help mitigate the small oscillations at steady state. Since there were many
#  discrepancies between our model/assumptions and physical system, we had to determine
#  the gains experimentally – using MATLAB to get a "ballpark" of gains and honing the
#  numbers through trial and error. Nonetheless, we are satisfied with the amount of tuning
#  we were able to achieve in a week. We would also like to do further research on how PI
#  control can be implemented in state space./n/n
# 
#  <b>Filtering</b> - In addition to adjusting the gains of the system, we believe
#  that we could achieve much smoother results by implementing a Kalman Filter. A Kalman
#  Filter, aka linear quadratic estimation (LQR) is a sort of algorithm that uses a series
#  of measurements observed over time to produce estimates of unknown variables that tend
#  to be more accurate than single raw measurements alone. A Kalman Filter could help reduce
#  the amount of noise that comes from the resistive touch panel readings, as well as the
#  encoder readings.\n\n
#
#  <b>Motors</b> - If we were to design a similar system again, from the ground up,
#  we would move the motors from the base of the platform to the U-joint at the center
#  of the platform. By moving the motors to the U-joint, the position of the platform could
#  be actuated directly, reducing the amount of resistance/backlash caused by pulleys
#  and excess joints. Additionally, the motion of the platform could be controlled linearly,
#  as compared to the non-linear motion of the motor arms. However, there are a few notable
#  drawbacks with this implementation. First, the motors would have to be gearless, and much
#  more powerful. Geared motors have backlash and would dramatically affect the control of
#  the system (especially around equilibrium). Second, the system would have to be redesigned
#  from the ground up. Altogether, a new system that uses motors located directly at the
#  U-joint would be much more accurate, but also cost much more.\n\n
#
#  <b>Resistive Touch Panel</b> - One of the biggest bottlenecks that we encountered with
#  this project is the resistive touch panel. As it stands, the touch panel relies on the
#  constant weight of the ball. When the platform makes a dramatic change, causing the apparent
#  weight of the ball to decrease, the touch panel cannot detect the ball and the measurements are
#  invalid. As a result, the ball cannot be controlled reliably. Implementing computer
#  vision software, such as OpenCV, could provide more reliable object tracking measurements.
#  However, the implementation of such software has its own drawbacks. The camera, and the
#  environment, would have to be carefully calibrated so that the ball could be tracked reliably.
#  In addition, the computer vision software would have to account for visual distortions that
#  occur due to the lens of the camera, as well as the depth of the image. Nonetheless, computer
#  vision has made dramatic advancements in recent years and many of these problems can be mitigated.
#  The one major advantage that the resistive touch panel has over computer vision is its latency.
#  We were able to scan the touch panel for the ball’s position in around 500 μs. With optimized code,
#  we expect the equivalent process with computer vision to have a latency of approximately 8 ms. 
#  Of course, the material of the ball could always be changed to account for the behavior of the touch panel.\n\n
#
#  <b>The Ball</b> - The most bang for your buck improvement that could be
#  made to the system is upgrading to a steel bearing. Some of the most significant
#  sources of instability cane from the touch panel’s inability to read the ball’s
#  position due to the low weight. Another advantage of using a steel bearing is the
#  smoothness. The current ball has flat spots that cause the system dynamics to change.
#
#  @section lab0x09_FSD Task Controller State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram for the task controller.
#  @image html termProjectFSD.png
#  
#  @section lab0x09_Task_Diagram Controller Task Diagram
#  Here is the task diagram that illustrates the interactions between the tasks.
#  @image html termProjectTaskDiagram.png
#
#  @section lab0x09_video_demo Video Demo
#  Here is the link to the video demonstration of the system: https://youtu.be/pnaUqpFKSWc
#  @section lab0x09_documentation Documentation
#  Here is the documentation of this lab package: Lab0x09
#  @section lab0x09_source Source Code 
#  Here is the link to the repository for the source code this lab package: https://bitbucket.org/mitchellkcarroll/term-project/src/master/
#  - - -
#  @author Grant Gallagher
#  @author Mitchell Carroll
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#  - - - 
#  @section bno055 Documentation for the IMU driver
#  It is important to note that the driver code for the BNO055 was
#  was written by Radomir Dopieralski, and ported to Python by
#  Peter Hinch 2019.\n
#  Here is a link to the documentation and source code repository
#  for the BNO055 driver classes: https://github.com/micropython-IMU/micropython-bno055.
#  @copyright Copyright © Peter Hinch 2019