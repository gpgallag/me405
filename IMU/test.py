# bno055_test.py Simple test program for MicroPython bno055 driver

# Copyright (c) Peter Hinch 2019
# Released under the MIT licence.

import machine
import time
from bno055 import *

def task_IMU():
    # Pyboard hardware I2C
    i2c = machine.I2C(1)

    imu = BNO055(i2c)
    
    while True:
        # if not calibrated:
        #     calibrated = imu.calibrated()
        #     print('Calibration required: sys {} gyro {} accel {} mag {}'.format(*imu.cal_status()))
        # print('Temperature {}°C'.format(imu.temperature()))
        # print('Mag       x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.mag()))
        #print('Gyro      x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.gyro()))
        #print('Accel     x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.accel()))
        #print('Lin acc.  x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.lin_acc()))
        # print('Gravity   x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.gravity()))
        # print('Heading     {:4.0f} roll {:4.0f} pitch {:4.0f}'.format(*imu.euler()))
        
        print(*imu.euler())
        pyb.delay(100)
        print("\x1B\x5B2J")
        print("\x1B\x5BH")
        
        return pyb
    
task_IMU()
