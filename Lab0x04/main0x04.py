'''
@file main0x04.py

@brief This file contains the main script used to measure and plot temperature data.

@details This script is specifically designed to interface with an MCP9808 temperature
         sensor driver via I2C communications over UART.

@package Lab0x04

@author Grant Gallagher

@date February 11, 2021
'''

import pyb
from pyb import I2C
import utime   
from mcp9808 import MCP9808

## The debug state of the main script. When set to "True" it will print various
#  debugging statements.
debug = True

# - - - - - - - - - Internal - - - - - - - - - 
## A pyb ADC object initialized to 12-bit resolution.
adc = pyb.ADCAll(12, 0x70000) # 0x70000 means only pins 16...18 active.
adc.read_core_vref() # Measure the Nucleo reference voltage (calibration).


# - - - - - - - - - Sensor - - - - - - - - - 
## An I2C object that declares the Nucleo as a master device.
I2C_OBJECT = I2C(1, I2C.MASTER, baudrate=115200)

## A constant defining the address of the MCP9808 temperature sensor (0-7)
MCP9808_ADDRESS = 0

## A MCP9808 sensor driver object.
sensor = MCP9808(I2C_OBJECT, MCP9808_ADDRESS)


# - - - - - - - - - Timing - - - - - - - - - 
## The number of milliseconds between runs of the task.
interval = 60*1000

## The starting time of data collection, in milliseconds since epoch.
start_time = utime.ticks_ms()
## The current time of the data collection loop, in milliseconds since epoch.
current_time = 0
## The next time to collect a datapoint, in milliseconds since epoch.
next_time = start_time + interval

## The number of milliseconds for the timeout of the program (8 hours).
end_time = start_time + 1000*60*60*8 


# - - - - - - - - - Main Script - - - - - - - - - 
if sensor.check() == True: 
    print('Success!')
    print('Recording temperature data...')
    print('To stop recording at any time, press CTRL+C.\n\n')
    
    with open ('Temperature_Data.csv', 'w') as file:   
        while current_time < end_time:
            current_time = utime.ticks_ms()
            if current_time >= next_time:
                # Calculate current time of datapoint
                ## The time since the start of data collection, in seconds.
                elapsed_time = utime.ticks_diff(utime.ticks_ms(), start_time) / 1000
                
                # Retrieve the current internal MCU temperature datapoints
                ## The core temperature of the Nucleo MCU in degrees Celsius.
                core_temp = adc.read_core_temp()
                
                ## The ambient temperature read by the MCP9808 in degrees Celsius.
                amb_temp_c = sensor.celsius()
                
                ## The ambient temperature read by the MCP9808 in degrees Farenheit.
                amb_temp_f = sensor.farenheit()
                
                if debug == True:
                    print('Current time (s): ' + str(elapsed_time))
                    print('Core Temp (C)   : ' + str(core_temp))
                    print('Ambient Temp (C)   : ' + str(amb_temp_c))
                    print('Ambient Temp (F)   : ' + str(amb_temp_c))
                    print('')
                    
                    
                    
                file.write('{:},{:},{:}\n'.format(elapsed_time, core_temp, amb_temp_c))
                
                # Increase the time value for the next interval
                next_time = utime.ticks_add(next_time, interval)
else:
    print('Error.'
          'Sensor not found.')
    