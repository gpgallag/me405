'''
@file mcp9808.py

@brief A temperature sensor driver class.

@details This file contains a class object that is used to drive a MVP9808 breakout board.

@package Lab0x04

@brief This package contains main0x04.py and mcp9808.py.

@author Grant Gallagher

@date February 11, 2021
'''
import pyb
from pyb import I2C

class MCP9808:
    '''
    @brief   A driver class for a MCP9808 breakout board.
    @details This class can be used to create a MCP9808 driver object, which is used
             to interface with the MCP9808 breakout board via I2C communication. This class
             contains functions that can: check if the temperature sensor is connected properly
             (comparing IDs), return the ambient temperature in degrees Celsius, and return the 
             temperature in degrees Farenheit.
            
    '''
    
    # Register Addresses as per Datasheet pg. 16
    
    ## RFU, reserved for future user (Read-Only register)
    REGISTER_RFU     = 0
    
    ## Configuration register
    REGISTER_CONFIG  = 1
    
    ## Alert Temperature Upper Boundary Trip register (T_upper)
    REGISTER_T_UPPER = 2
    
    ## Alert Temperature Lower Boundary Trip register (T_lower)
    REGISTER_T_LOWER = 3
    
    ## Critical Temperature Trip register (T_crit)
    REGISTER_T_CRIT  = 4
    
    ## Temperature register (T_a)
    REGISTER_T_A     = 5
    
    ## Manufacturer ID register
    REGISTER_MFG_ID  = 6
    
    ## Device ID/Revision register
    REGISTER_DEV_ID  = 7
    
    ## Resolution register
    REGISTER_RES     = 8
    
    def __init__(self, I2C_object, MCP9808_address):
        '''
        @brief The constructor for the MCP9808 driver object.
        @param I2C_object      The I2C object bound to the communication bus.
        @param MCP9808_address An integer 0-7 representing the communication bus
                               address of the sensor.
        '''   
        ## The I2C object bound to the communication bus.
        self.I2C = I2C_object
        
        ## An integer 0-7 representing the communication bus address of the sensor.
        self.MCP9808_address = 0b11000 + MCP9808_address
        
        ##A buffer used to receive 2 bytes of data from the I2C communication.
        self.buffy = bytearray(2)
        
        ## Manufacturer ID for the MCP9808 sensor.
        self.mfg_ID = 0x0054
        
    def check(self):
        '''
        @brief         Checks the sensor I2C address.
        @details       Verifies that the sensor is attached at the given bus address by
                       checking that the value in the manufacturer ID register is correct.
        @return sucess A boolean representing whether the manufacturer ID register is correct.
        '''
        # Checks the manfufacturer ID of the sensor through I2C.
        self.I2C.mem_read(self.buffy, self.MCP9808_address, self.REGISTER_MFG_ID) # READ command
        
        # A boolean representing whether the manufacturer ID register is correct.
        success = self.mfg_ID == self.buffy[1]
        
        return success
        
    def celsius(self):
        '''
        @brief  Measures the temperature in degrees Celsius
        @return temperature_C A float that contains the ambient temperature reading in degrees Celcius.
        '''  
        # !The majority of this code has been copied from pg. 25 of the MCP9808!
        self.I2C.mem_read(self.buffy, self.MCP9808_address, self.REGISTER_T_A) # READ command
        
        upper_byte = self.buffy[0] # Read 8 bits
                                   # and send ACK bit
        lower_byte = self.buffy[1] # Read 8 bits
                                   # and send NAK bit
        
        upper_byte = upper_byte & 0x1F     # Clear flag bits
        
        if ((upper_byte & 0x10) == 0x10): # T_A < 0°C
            upper_byte = upper_byte & 0x0F # Clear SIGN
            temperature_C = 256 - (upper_byte * 16 + lower_byte / 16)
        else: # T_A >= 0°C
            temperature_C = (upper_byte * 16 + lower_byte / 16);
            
        return temperature_C
    
    
    def farenheit(self):
        '''
        @brief  Measures the temperature in degrees Farenheit.
        @return temperature_F A float that contains the ambient temperature reading in degrees Farenheight.
        '''
        temperature_F = (self.celsius() * 9 / 5) + 32
        
        return temperature_F



# # Test code section
# if __name__ == '__main__':
#     ## A UI_Back task object
#     # Construct an I2C object that declares the Nucleo as a master device.
#     I2C_OBJECT = I2C(1, I2C.MASTER, baudrate=115200)
#     # A constant defining the address of the MCP9808 temperature sensor (0-7)
#     MCP9808_ADDRESS = 0
#     # A MCP9808 sensor driver object.
#     sensor = MCP9808(I2C_OBJECT, MCP9808_ADDRESS)
    
#     print(I2C_OBJECT.scan()) # scan for slaves on the bus, returning
#                              #   a list of valid addresses
#     print(I2C_OBJECT.is_ready(24))
#     if True:
        
#         # Run forever
#         while True:
#             print('Temperature in Celsius   : ' + str(sensor.celsius()))
#             print('Temperature in Farenheit : ' + str(sensor.farenheit()))
#             print('')
#             pyb.delay(1000)
            