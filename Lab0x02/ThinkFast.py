'''
@file ThinkFast.py

@brief A script that simulates a user-reaction timer.

@details This file contains a program that runs continuously to test a user's reaction
         time. At startup, the program prompts the user of the objective of the test.
         Once started, there will be a psuedo-random 2-3 second delay before an LED
         onboard the Nucleo turns on. Once the LED turns, a clock with a 1 MHz timer
         is started and records the reaction time of the user. If the button is pressed
         too early, the reaction time is invalid. If the user takes longer than 1 second
         to press the button, the default time recorded is 1 second. The program will
         run continuously until the user presses CTRL+C or the Nucleo loses power.
         When the user exits the program, the average reaction time, in milliseconds
         (with microsecond precision), will be printed.

@package Lab0x02

@brief This package contains ThinkFast.py
       
@author Grant Gallagher

@date January 28, 2021
'''

# Import necesarry libraries
import random
import utime
import micropython
import pyb

# An emergency buffer to store errors that are thrown inside ISR.
micropython.alloc_emergency_exception_buf(200)

## A timer object set to a clock frequency of 1 MHz, or a period of 1 microsecond
timer = pyb.Timer(5, prescaler = 79, period=0x7FFFFFFF) # Clock frequency == ("CPU clockrate") / ("prescaler" + 1)

## A pin object initialized to the blue User button onboard the Nucleo.
button = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)

## A pin object initialized to the green LED onboard the Nucleo.
LED = pyb.Pin(pyb.Pin.cpu.A5, mode = pyb.Pin.OUT)

## An integer indicating the time in which the User button is pressed during recording (microseconds).
reaction_time = 0x7FFFFFFF

## A boolean representing whether the User button is pressed legally during recording.
button_pressed = False

## A boolean representing whether the User button is pressed early during recording.
button_pressed_early = False

## A list containing the valid reaction times of recording runs.
reaction_list = []

## An integer indicating the starting time of the recording run.
start_time = 0

def callback(IRQ):
    ''' 
    @brief An interupt callback function.
    @details An interrupt handler that is called when the User button is pressed.
             Records the time in which the user button was pressed, in microseconds,
             with respect to the start of the clock timer.

    @param IRQ The desired physical interupt line.
    '''
    # Declare global variables for callback function
    global reaction_time
    global button_pressed
    global button_pressed_early
    global start_time
    global delay_time
    
    reaction_time = timer.counter() # Records the reaction time
    
    button_pressed = True # Records that the button was pressed
    
    if utime.ticks_diff(utime.ticks_us(),start_time) < delay_time: # Records if the button was pressed too early
            button_pressed_early = True
            
## The callback for the user button that activates on the falling edge.
extint = pyb.ExtInt(button, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback)


try:
    print('------------------------------------------------------------------------')
    print('Welcome to the reaction timer!\n'
          'When the LED turns on, your job is to press the BLUE\n'
          'button as quickly as possible.\n'
          'The program will run continuously until you press CTRL+C to stop.\n'
          'At that point your average reaction time will be printed. Are you ready?\n')
    print('Press ENTER to begin.')
    print('------------------------------------------------------------------------')
    input('\n')
    while True:
        start_time = utime.ticks_us() # The starting time of the run.
        
        # Delays the start of the reaction test
        print('A new run is about to start in 2 to 3 seconds.')
        print('Get ready...\n')
        ## A random time, between 2-3 seconds, to delay the start of the LED.
        delay_time = random.randint(2000000,3000000)
        pyb.udelay(delay_time)
        
        # The start of when the LED turns on after a 2-3s delay
        print('NOW!\n')
        LED.on()
        timer.counter(0)
        pyb.delay(1000)
        LED.off()

        # Button Pressed too early
        if (button_pressed == True) and (button_pressed_early == True):
            print('You pressed the button too early.')
            print('No reaction time has not been recorded. Try again.\n')
        
        # Button pressed in an appropriate amount of time
        elif button_pressed == True and button_pressed_early == False:
            reaction_list.append(reaction_time)
            print('Your reaction time was ' + str(reaction_time/1000.0) + ' milliseconds.') 
            print('This reaction time has been recorded.\n')
        
        # Button pressed too late
        else:
            reaction_list.append(1000000)
            print('You took longer than 1 second to respond.')
            print('Your reaction has been recorded as 1 second.\n')
            pass

        # Resets all of the parameters
        reaction_time = 0x7FFFFFFF
        button_pressed = False
        button_pressed_early = False

# Exit condition: Returns the user the average reaction time if available.        
finally:
    reaction_list = [time for time in reaction_list if time != 0x7FFFFFFF]
    
    if len(reaction_list) > 0:
        ## The average reaction time of the user over the course of the program (ms)
        average_reaction_time = (sum(reaction_list) / len(reaction_list))
        print('\n---------------------------------------------------------------')
        print('You pressed CTRL+C to exit the program.')
        print('Your average reaction time was ' + str(average_reaction_time / 1000.0) + ' milliseconds.')
        print('---------------------------------------------------------------\n\n')
    else:
        print('\n---------------------------------------------------------------')
        print('\nThere were no valid reaction recordings.\n\n')
        print('---------------------------------------------------------------\n\n')